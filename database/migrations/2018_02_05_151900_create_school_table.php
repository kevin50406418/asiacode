<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school', function (Blueprint $table) {
            $table->string('id',10)->primary()->comment('代碼');
            $table->string('name')->comment('學校名稱');
            $table->string('address')->comment('學校地址');
            $table->string('website')->comment('學校網址');
            $table->string('phone')->comment('學校電話');
            $table->string('student_email')->nullable()->comment('學生信箱');
            $table->boolean('type')->comment('公(0)/私(1)立');
            $table->boolean('open')->default(0)->comment('是否開放報名');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school');
    }
}
