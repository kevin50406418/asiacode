<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamCodingLangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_coding_lang', function (Blueprint $table) {
            $table->unsignedInteger('exam_id')->comment('考試編號');
            $table->unsignedInteger('lang_id')->comment('程式語言編號');
            $table->timestamps();

            $table->primary(['exam_id','lang_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_coding_lang');
    }
}
