<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->comment('帳號');
            $table->string('name_zh_tw')->comment('中文姓名');
            $table->string('last_name_en')->comment('英文姓');
            $table->string('first_name_en')->comment('英文名');
            $table->string('email')->unique()->comment('信箱');
            $table->string('password')->comment('密碼');
            $table->string('school_id')->comment('學校代碼');
            $table->string('dept_id')->comment('學系代碼');
            $table->string('edu_level')->comment('學制');
            $table->string('student_id')->comment('學號');
            $table->tinyInteger('sex')->comment('性別');
            $table->rememberToken();
            $table->timestamps();

            $table->unique(['school_id', 'student_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
