<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExamSignupAddKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exam_signup', function (Blueprint $table) {
            $table->renameColumn('cert_key', 'cert_key_1')->comment('證書金鑰1')->change();
            $table->string('cert_key_2')->nullable()->comment('證書金鑰2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_signup', function (Blueprint $table) {
            $table->renameColumn('cert_key_1', 'cert_key')->comment('證書金鑰')->change();
            $table->dropColumn('cert_key_2');
        });
    }
}
