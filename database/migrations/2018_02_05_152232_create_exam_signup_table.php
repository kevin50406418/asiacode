<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamSignupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_signup', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('exam_id')->comment('考試編號');
            $table->unsignedInteger('user_id')->comment('使用者編號');
            $table->unsignedInteger('lang_id')->comment('程式語言編號');
            $table->string('name_zh_tw')->comment('中文姓名');
            $table->string('last_name_en')->comment('英文姓');
            $table->string('first_name_en')->comment('英文名');
            $table->string('school_id')->comment('學校代碼');
            $table->string('dept_id')->comment('學系代碼');
            $table->string('student_id')->comment('學號');
            $table->string('edu_level')->comment('學制');
            $table->string('pc2_account',64)->nullable()->comment('PC 2 帳號');
            $table->string('pc2_password',64)->nullable()->comment('PC 2 密碼');
            $table->integer('solve_problem')->nullable()->default(0)->comment('解題數, -1 為沒來考試');
            $table->string('cert_id')->nullable()->comment('證書字號');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_signup');
    }
}
