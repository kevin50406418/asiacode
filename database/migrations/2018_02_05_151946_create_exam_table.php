<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('名稱');
            $table->string('place')->comment('考試地點');
            $table->text('desc')->comment('考試說明');
            $table->dateTime('time_start')->comment('考試時間：開始時間');
            $table->dateTime('time_end')->comment('考試時間：結束時間');
            $table->dateTime('signup_start')->comment('報名時間：開始時間');
            $table->dateTime('signup_end')->comment('報名時間：截止時間');
            $table->integer('pass_threshold')->nullable()->comment('通過門檻');
            $table->dateTime('publish_time')->comment('成績公布時間');
            $table->text('problem_note')->comment('考題註記');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam');
    }
}
