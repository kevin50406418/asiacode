<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolDeptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_dept', function (Blueprint $table) {
            $table->string('id',10)->primary()->comment('科系代碼');
            $table->string('name')->comment('科系名稱');
            $table->string('school_id',10)->comment('學校代碼');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_dept');
    }
}
