<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExamSignupAddIdNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exam_signup', function (Blueprint $table) {
            $table->string('id_number')->comment('身分證');
            $table->string('cert_key')->nullable()->comment('證書金鑰');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_signup', function (Blueprint $table) {
            $table->dropColumn('id_number');
            $table->dropColumn('cert_key');
        });
    }
}
