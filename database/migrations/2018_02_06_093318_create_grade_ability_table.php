<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradeAbilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grade_ability', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('exam_id')->comment('考試編號');
            $table->string('slug',5)->comment('階級代碼');
            $table->string('cert_title_en')->comment('證書顯示名稱(英)');
            $table->string('cert_title_zh_tw')->comment('證書顯示名稱(中)');
            $table->text('desc')->comment('能力指標說明');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grade_ability');
    }
}
