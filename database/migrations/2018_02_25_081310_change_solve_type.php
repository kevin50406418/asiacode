<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSolveType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exam', function (Blueprint $table) {
            $table->decimal('pass_threshold',3,1)->nullable()->comment('通過門檻')->change();
        });

        Schema::table('exam_signup', function (Blueprint $table) {
            $table->decimal('solve_problem',3,1)->nullable()->default(0)->comment('解題數, -1 為沒來考試')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam', function (Blueprint $table) {
            $table->integer('pass_threshold')->nullable()->comment('通過門檻')->change();
        });

        Schema::table('exam_signup', function (Blueprint $table) {
            $table->integer('solve_problem')->nullable()->default(0)->comment('解題數, -1 為沒來考試')->change();
        });
    }
}
