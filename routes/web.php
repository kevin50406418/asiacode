<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ExamController@index')->name('home');
Route::get('home', 'ExamController@index')->name('home.index');
Route::get('exam', 'ExamController@index')->name('exam.index');
Route::get('show/{id}', 'ExamController@show')->name('exam.show');
Route::get('cert/{key}', 'CertController@show')->name('cert.index');
Route::post('cert/{key}', 'CertController@verify')->name('cert.verify');
Route::get('problem', 'ProblemController@index')->name('problem.index');
Route::get('problem/{id}', 'ProblemController@show')->name('problem.show');

/**
 * Member
 */
Route::group([
    'middleware' => ['auth'],
], function (){
    Route::post('exam/{id}/signup', 'ExamController@store')->name('exam.store');
    //    Route::get('exam/{id}/certificate', 'ExamController@certificate')->name('exam.certificate');
    Route::get('exam/{id}/cancel', 'ExamController@cancel')->name('exam.cancel');
    Route::get('profile', 'PorfileController@index')->name('profile.index');
    Route::post('profile/passwd', 'PorfileController@passwd')->name('profile.passwd');
});

/**
 * Authentication Routes
 */
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
Route::get('email-verification/error',
    'Auth\RegisterController@getVerificationError')->name('email-verification.error');
Route::get('email-verification/check/{token}',
    'Auth\RegisterController@getVerification')->name('email-verification.check');

/**
 * API
 */
Route::group([
    'prefix'    => 'api',
    'as'        => 'api.',
    'namespace' => 'Api'
], function (){
    Route::get('exam', 'ExamController@all')->name('exam');
    Route::get('dept', 'DeptController@get')->name('dept');
    Route::get('problem', 'ProblemController@all')->name('problem');
});


/**
 * Dashboard
 */
Route::group([
    'middleware' => ['auth', 'role:admin'],
], function (){
    Route::get('api/signup/{id}', 'Api\ExamController@signup')->name('api.signup')->where('id', '[0-9]+');
    Route::get('api/user/all', 'Api\UserController@all')->name('api.user.all');
    Route::get('api/school/all', 'Api\SchoolController@all')->name('api.school.all');
    Route::get('api/problems', 'Api\ProblemController@dashboard')->name('api.problem.all');
    Route::get('api/exams', 'Api\ExamController@admin')->name('api.exam.all');

    Route::group([
        'prefix'    => 'dashboard',
        'as'        => 'dashboard.',
        'namespace' => 'Dashboard'
    ], function (){
        Route::post('exam/threshold/{id}', 'ExamController@update_threshold')->name('exam.threshold')->where('id',
            '[0-9]+');

        Route::get('exam-export/list/{id}', 'ExamExportController@index')->name('exam-export.list');
        Route::get('exam-export/pc2/{id}', 'ExamExportController@pc2')->name('exam-export.pc2');

        Route::post('exam/score/enter/{id}', 'ExamScoreController@index')->name('exam-score.index');
        Route::post('exam/score/update/{id}', 'ExamScoreController@store')->name('exam-score.store');
        Route::get('exam/score/cert/{id}', 'ExamScoreController@cert')->name('exam-score.cert');
        Route::get('exam/cert/{id}', 'ExamScoreController@cert_file')->name('exam-score.cert_file');

        Route::resource('grade-ability', 'GradeAbilityController')->only(['edit', 'update']);
        Route::resource('exam-problem', 'ExamProblemController',
            ['parameters' => ['exam-problem' => 'id']])->only(['show', 'update', 'destroy']);
        Route::resource('exam', 'ExamController');
        Route::resource('user', 'UserController');
        Route::resource('school', 'SchoolController');
        Route::resource('problem', 'ProblemController')->only(['index', 'show']);
    });

});

Route::get('datatable', 'DatatableController')->name('js.datatable');

// Password Reset Routes...
//Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
//Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
//Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
//Route::post('password/reset', 'Auth\ResetPasswordController@reset');
