$(document).ready(function () {
	$('.navbar-toggle').on('click', function (event) {
		$(this).toggleClass('open');
		$('#navigation').slideToggle(400);
	});

	$('.navigation-menu>li').slice(-1).addClass('last-elements');

	$('.navigation-menu li.has-submenu a[href="#"]').on('click', function (e) {
		if ($(window).width() < 992) {
			e.preventDefault();
			$(this).parent('li').toggleClass('open').find('.submenu:first').toggleClass('open');
		}
	});

	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();
	$('.toggle-search').on('click', function () {
		var targetId = $(this).data('target');
		var $searchBar;
		if (targetId) {
			$searchBar = $(targetId);
			$searchBar.toggleClass('open');
		}
	});
	$('#status').fadeOut();
	$('#preloader').delay(350).fadeOut('slow');
	$('body').delay(350).css({
		'overflow': 'visible'
	});
});
