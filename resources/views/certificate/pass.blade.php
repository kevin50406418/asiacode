<!DOCTYPE html>
<html>
<head>
	<title>Certificate of Java Programming</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	<link href="{{ asset('css/pdf.css') }}" rel="stylesheet">
	<style>
		@font-face {
			font-family: 'kaiu';
			font-style: normal;
			font-weight: normal;
			src: url('{{ asset('fonts/kaiu.ttf') }}') format('truetype');
		}

		html, body {
			height: 95%;
			margin: 0 auto;
			padding: 2%;
		}

		body {
			width: 95%;
		}

		h1.title {
			font-size: 64px;
			font-family: 'Lobster', sans-serif;
		}

		h1.company_title {
			font-size: 36px;
			font-family: 'kaiu', sans-serif;
			line-height: 64px;
		}

		.text-center {
			text-align: center;
		}

		.header {
			font-size: 28px
		}

		.hr {
			 border: 1px solid #c51515;
			 margin: 0 auto;
		 }

		.hr_2_1 {
			border: 1px solid #ed7d31;
			margin: 0 auto;
			width: 85%;
		}
		.hr_2_2 {
			border: 2.5px solid #ed7d31;
			margin: 0 auto;
			width: 85%;
		}

		.m-t-10 {
			margin-top: 10px;
		}

		.m-t-20 {
			margin-top: 20px;
		}

		.m-t-3x {
			margin-top: 3em;
		}

		.left {
			font-family: 'Source Sans Pro', sans-serif;
			font-size: 36px;
		}

		.left .now_text {
			font-size: 28px;
		}

		.left .name_en {
			font-size: 52px;
			line-height: 80px;
		}

		.right {
			font-family: 'kaiu', sans-serif;
			font-size: 30px;
		}

		.right .name_zh_tw {
			font-size: 48px;
			line-height: 75px;
		}
		.right .grade_ably{
			width: 80%;
			margin: 0 auto;
		}

		.footer{
			margin-top: 2em;
			font-family: 'Source Sans Pro', sans-serif;
			font-size: 28px;
		}
	</style>
</head>
<body>
<div class="text-center header">
	<h1 class="title">Certificate of <span class="text-primary">{{ $langs[$signup->lang_id]->lang->lang }}</span> Programming</h1>
	<h1 class="company_title text-primary">捷頂科技有限公司</h1>
</div>
<div class="hr"></div>
<div class="m-t-20"></div>
<div class="container">
	<div class="row">
		<div class="col-xs-6 left text-center">
			<p>This is to certificate that</p>
			<span class="name_en">{{ $signup->sex ? 'Ms' : 'Mr' }}. <strong class="text-primary">{{ $signup->first_name_en }} {{ $signup->last_name_en }}</strong></span>
			<p>
				has completed all of the prescribed courses and has successfully passed the examination of the certificate body
			</p>
			<p class="now_text">now to be recognized as a </p>
			<strong class="grade_name text-primary">{{ str_replace(['{code}'],[$langs[$signup->lang_id]->lang->lang],$grade->cert_title_en) }}</strong>
		</div>
		<div class="col-xs-6 right text-center">
			<p class="name_zh_tw text-primary">{{ $signup->name_zh_tw }} {{ $signup->sex ? '女士' : '先生' }}</p>
			<p>參加了軟體開發的</p>
			<p class="text-primary">{{ str_replace(['{code}'],[$langs[$signup->lang_id]->lang->lang],$grade->cert_title_zh_tw) }}</p>
			<p>通過測驗，成績合格，特此認證</p>
			<div class="m-t-20"></div>
			<p>獲得本證書即具備下列能力</p>
			<div class="hr_2_1"></div>
			<div style="margin-bottom: 5px"></div>
			<div class="hr_2_2"></div>
			<div style="margin-bottom: 5px"></div>
			<div class="text-left grade_ably">
				{!! $grade->desc !!}
			</div>
		</div>
	</div>
	<div class="m-t-3x"></div>

</div>
<div class="row footer">
	<div class="col-xs-4">
		ID No:
	</div>
	<div class="col-xs-4">
		AAD No: {{ $grade->slug }}{{ $langs[$signup->lang_id]->lang->slug }}{{ \Carbon\Carbon::parse($exam->publish_time)->format('Y') }}{{ str_pad($signup->cert_id, 4, "0", STR_PAD_LEFT) }}
	</div>

	<div class="col-xs-4 text-right">
		Issue Date: {{ \Carbon\Carbon::parse($exam->publish_time)->format('F j, Y') }}
	</div>
</div>
</body>
</html>