@extends('layouts.app')

@section('page-title', '證照驗證')

@section('content')
	<div class="p-3">
		證書驗證碼：<code>{{ \Uuid::generate(5,$signup->id_number.$signup->cert_key_2, \Uuid::NS_DNS) }}</code>
	</div>
@endsection
