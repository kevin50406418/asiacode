@extends('layouts.app')

@section('page-title', '證照驗證')

@section('content')
	<div class="p-3">
		<form class="form-horizontal m-t-20" method="POST" action="{{ route('cert.verify',$key) }}">
			{{ csrf_field() }}

			<div class="form-group">
				<label for="id_number">身分字號*</label>
				<input id="id_number" type="text" class="form-control {{ $errors->has('id_number') ? 'is-invalid' : '' }}" name="id_number" value="{{ old('id_number') }}" required autofocus>
				@if ($errors->has('id_number'))
					<div class="invalid-feedback"> {{ $errors->first('id_number') }} </div>
				@endif
			</div>


			<div class="form-group text-center row m-t-20">
				<div class="col-12">
					<button class="btn btn-info btn-block waves-effect waves-light" type="submit">證照驗證</button>
				</div>
			</div>

		</form>
	</div>
@endsection
