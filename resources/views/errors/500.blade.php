@extends('layouts.errors')

@section('page-title', '404')

@section('content')
	<div class="card">
		<div class="card-block">

			<div class="ex-page-content text-center">
				<h1 class="">500</h1>
				<h3 class="">Internal Server Error</h3><br>

				<a class="btn btn-info mb-5 waves-effect waves-light" href="{{ route('home.index') }}">Back to Home</a>
			</div>

		</div>
	</div>
@endsection
