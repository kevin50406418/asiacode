@extends('layouts.app')

@section('page-title','考試管理')

@section('content')
	<div class="m-3">
		<a class="btn btn-primary" href="{{ route('dashboard.exam.create') }}">建立考試</a>
	</div>

	<table id="exam" class="table table-striped table-bordered">
		<thead>
		<tr>
			<th>#</th>
			<th>名稱</th>
			<th>報名時間</th>
			<th>考試時間</th>
			<th>操作</th>
		</tr>
		</thead>
		<tbody>
		@foreach($exams as $exam)
			<tr>
				<td>{{ $exam->id }}</td>
				<td>
					{{ $exam->name }}
				</td>
				<td>
					{{ $exam->signup_start->format('Y-m-d H:i') }} ~ {{ $exam->signup_end->format('Y-m-d H:i') }}
				</td>
				<td>
					{{ $exam->time_start->format('Y-m-d H:i') }} ~ {{ $exam->time_end->format('Y-m-d H:i') }}
				</td>
				<td>
					<a href="{{ route('dashboard.exam.edit',$exam->id) }}">修改</a>
					<a href="{{ route('dashboard.exam.show',$exam->id) }}">名單</a>
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
@endsection

@push('script')
	<script>
		$(function () {
			$('#exam').DataTable({
				processing: true,
				serverSide: true,
				ajax: '{{ route('api.exam.all') }}',
				columns: [
					{data: 'index'},
					{data: 'name'},
					{data: 'time'},
					{data: 'signup_time'},
					{data: 'action', name: 'action', orderable: false, searchable: false}
				],
				language: {
					"url": "{{ route('js.datatable') }}"
				}
			});
		});
	</script>
@endpush