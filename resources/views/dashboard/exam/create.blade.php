@extends('layouts.app')

@section('page-title','建立考試資訊')

@section('content')
	@includeWhen($errors->any(), 'component.alert-validation', ['errors' => $errors])
	<form method="POST" action="{{ route('dashboard.exam.store') }}">
		{{ csrf_field() }}
		<h3>考試資訊</h3>
		<hr class="my-4">
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="name">名稱*</label>
				<input id="name" type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
				@if ($errors->has('name'))
					<div class="invalid-feedback"> {{ $errors->first('name') }} </div>
				@endif
			</div>
			<div class="form-group col-md-6">
				<label for="place">地點*</label>
				<input id="place" type="text" class="form-control {{ $errors->has('place') ? 'is-invalid' : '' }}" name="place" value="{{ old('place') }}">
				@if ($errors->has('place'))
					<div class="invalid-feedback"> {{ $errors->first('place') }} </div>
				@endif
			</div>
			<div class="form-group col-md-12">
				<label for="desc">考試說明*</label>
				<textarea id="desc" type="text" class="form-control {{ $errors->has('desc') ? 'is-invalid' : '' }}" name="desc">{{ old('desc') }}</textarea>
				@if ($errors->has('desc'))
					<div class="invalid-feedback"> {{ $errors->first('desc') }} </div>
				@endif
			</div>
			<div class="form-group col-md-6">
				<label for="time_start">考試時間：開始時間*</label>
				<date-picker v-model="time_start" :name="'time_start'" :config="config"></date-picker>
				@if ($errors->has('time_start'))
					<div class="invalid-feedback"> {{ $errors->first('time_start') }} </div>
				@endif
			</div>
			<div class="form-group col-md-6">
				<label for="time_end">考試時間：結束時間*</label>
				<date-picker v-model="time_end" :name="'time_end'" :config="config"></date-picker>
				@if ($errors->has('time_end'))
					<div class="invalid-feedback"> {{ $errors->first('time_end') }} </div>
				@endif
			</div>
			<div class="form-group col-md-6">
				<label for="signup_start">報名時間：開始時間*</label>
				<date-picker v-model="signup_start" :name="'signup_start'" :config="config"></date-picker>
				@if ($errors->has('time_start'))
					<div class="invalid-feedback"> {{ $errors->first('time_start') }} </div>
				@endif
			</div>
			<div class="form-group col-md-6">
				<label for="signup_end">報名時間：截止時間*</label>
				<date-picker v-model="signup_end" :name="'signup_end'" :config="config"></date-picker>
				@if ($errors->has('time_end'))
					<div class="invalid-feedback"> {{ $errors->first('time_end') }} </div>
				@endif
			</div>
			<div class="form-group col-md-6">
				<label for="pass_threshold">通過門檻</label>
				<div class="text-info">請建立完後再設定門檻值</div>
				<div class="text-danger">請小心設定, 大於等於門檻值將產生通過證書</div>
				{{--<input id="pass_threshold" type="text" class="form-control {{ $errors->has('pass_threshold') ? 'is-invalid' : '' }}" name="pass_threshold" value="{{ old('pass_threshold',0) }}">--}}
				{{--@if ($errors->has('pass_threshold'))--}}
					{{--<div class="invalid-feedback"> {{ $errors->first('pass_threshold') }} </div>--}}
				{{--@endif--}}
			</div>
			<div class="form-group col-md-6">
				<label for="publish_time">成績公布時間*</label>
				<date-picker v-model="publish_time" :name="'publish_time'" :config="config"></date-picker>
				@if ($errors->has('publish_time'))
					<div class="invalid-feedback"> {{ $errors->first('publish_time') }} </div>
				@endif
			</div>
		</div>
		<h3>考試設定</h3>
		<hr class="my-4">
		<div class="form-group">
			<label for="publish_time">考試程式語言*</label>
			@foreach($langs as $lang)
				<div class="custom-control custom-checkbox mr-sm-2">
					<input type="checkbox" name="exam_lang[]" class="custom-control-input" id="lang_{{ $lang->id }}" value="{{ $lang->id }}">
					<label class="custom-control-label" for="lang_{{ $lang->id }}">{{ $lang->lang }}</label>
				</div>
			@endforeach
			@if ($errors->has('publish_time'))
				<div class="invalid-feedback"> {{ $errors->first('publish_time') }} </div>
			@endif
		</div>
		<button type="submit" class="btn btn-primary">新增</button>
	</form>
@endsection

@push('js')
	<script src="https://cdn.ckeditor.com/4.8.0/basic/ckeditor.js"></script>
	<script src="https://cdn.ckeditor.com/4.8.0/basic/adapters/jquery.js"></script>
@endpush

@push('script')
	<script>
		$(function () {
			$( 'textarea#desc' ).ckeditor();
		});
		const app = new Vue({
			el: '#app',
			data: {
				time_start: '{{ old('time_start') }}',
				time_end: '{{ old('time_end') }}',
				signup_start: '{{ old('signup_start') }}',
				signup_end: '{{ old('signup_end') }}',
				publish_time: '{{ old('publish_time') }}',
				config: {
					format: 'YYYY-MM-DD HH:mm',
					locale: 'zh-tw',
					useCurrent: false,
					sideBySide: true,
					icons: {
						time: 'far fa-clock',
						date: 'far fa-calendar-alt',
						up: 'fas fa-chevron-up',
						down: 'fas fa-chevron-down',
						previous: 'fas fa-chevron-left',
						next: 'fas fa-chevron-right',
						today: 'fas fa-map-marker',
						clear: 'fas fa-trash',
						close: 'fas fa-times'
					}
				},
			},
		});
	</script>
@endpush