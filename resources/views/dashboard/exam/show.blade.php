@extends('layouts.app')

@section('page-title','考試名單')

@section('content')
	<h2 class="display-4">{{ $exam->name }}</h2>
	<hr class="my-4">
	<div>
		<i class="fas fa-map-marker-alt fa-fw"></i> 考試地點：{{ $exam->time_end }}
	</div>
	<div class="mb-3">
		<i class="fas fa-calendar-alt fa-fw"></i> 考試時間：{{ $exam->time_start }} ~ {{ $exam->time_end }}
	</div>
	<div class="card">
		<div class="card-body">
			{!! $exam->desc !!}
		</div>
	</div>
	<div class="mt-3 mb-3">
		<i class="fas fa-calendar-check"></i> 報名時間：{{ $exam->signup_start }} ~ {{ $exam->signup_end }}
	</div>

	@if( count($signups) != $pc2 )
		<div class="alert alert-danger">
			警告: 有報名者未有 PC<sup>2</sup> 帳號資訊
		</div>
	@endif

	@if( is_null($exam->pass_threshold) )
		<div class="alert alert-info">
			提示: 尚未設定通過門檻
		</div>
	@endif

	@includeWhen($errors->any(), 'component.alert-validation', ['errors' => $errors])

	<table class="table table-bordered text-center">
		<thead class="thead-dark">
		<tr>
			<th>報考人數</th>
			<th>PC<sup>2</sup> 產生人數</th>
			<th>PC<sup>2</sup> site</th>
			<th>平均題數</th>
			<th>通過門檻</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>{{ count($signups) }}</td>
			<td>{{ $pc2 }}</td>
			<td>site{{ $exam->id }}</td>
			<td>{{ $signups->pluck('solve_problem')->avg() }}</td>
			<td>
				<div class="input-group">
					<input id="pass_threshold" type="text" class="form-control" placeholder="尚未設定通過門檻" value="{{ $exam->pass_threshold }}">
					<div class="input-group-append">
						<button id="update-threshold" class="btn btn-outline-secondary" type="button">設定</button>
					</div>
				</div>
			</td>
		</tr>
		</tbody>
	</table>

	<canvas id="myChart" height="80"></canvas>

	<div class="mt-3 mb-3">
		<a href="{{ route('dashboard.exam-export.list', $exam->id) }}" class="btn btn-success">匯出名單</a>
		<a data-toggle="collapse" href="#collapsePC2Team" class="btn btn-danger">產生 PC<sup>2</sup> 匯入檔</a>
		<a href="{{ route('dashboard.grade-ability.edit', $exam->id) }}" class="btn btn-outline-info">成績說明設定</a>
		<a href="#collapseScore" class="btn btn-outline-primary" data-toggle="collapse" role="button">成績匯入</a>
		@if( !is_null($exam->pass_threshold) )
			<a href="{{ route('dashboard.exam-score.cert', $exam->id) }}" class="btn btn-outline-danger">證書產生</a>
			<a href="{{ route('dashboard.exam-score.cert_file', $exam->id) }}" class="btn btn-outline-danger">證書檔案</a>
		@endif
	</div>

	<div id="accordion">
		<div id="collapsePC2Team" class="collapse" data-parent="#accordion">
			<div class="card card-body">
				<p>
					<a href="{{ route('dashboard.exam-export.pc2', $exam->id) }}" class="btn btn-danger" onclick="return confirm('是否產生?\n注意:若之前有產生將會覆蓋!!')">下載 PC<sup>2</sup> 匯入檔</a>
				</p>

				<figure class="figure text-center">
					<img src="{{ asset('img/team-generate-1.PNG') }}" class="figure-img img-fluid rounded" alt="">
					<figcaption class="figure-caption text-center">1. 在開啟時，請先確認開啟的站點是否為
						<span class="badge badge-success">site{{ $exam->id }}</span></figcaption>
					<figcaption class="figure-caption text-center">2. 點擊
						<span class="badge badge-info">Generate</span> 按鈕
					</figcaption>
				</figure>
				<figure class="figure text-center">
					<img src="{{ asset('img/team-generate-2.PNG') }}" class="figure-img img-fluid rounded" alt="">
					<figcaption class="figure-caption text-center">3. 在匯入前，請先產生
						<span class="badge badge-success">{{ count($signups) }}</span> 個 team 帳號
					</figcaption>
				</figure>
				<figure class="figure text-center">
					<img src="{{ asset('img/team-generate-3.PNG') }}" class="figure-img img-fluid rounded" alt="">
					<figcaption class="figure-caption text-center">4. 點擊
						<span class="badge badge-info">Load</span> 按鈕，匯入帳號，並選擇系統(<span class="badge badge-success">site{{ $exam->id }}.txt</span>)產生的檔案匯入
					</figcaption>
				</figure>
				<figure class="figure text-center">
					<img src="{{ asset('img/team-generate-4.PNG') }}" class="figure-img img-fluid rounded" alt="">
					<figcaption class="figure-caption text-center">5. PC<sup>2</sup> 會看到匯入的名單，確認沒問題後，點擊
						<span class="badge badge-info">Accept</span> 按鈕，完成匯入
					</figcaption>
				</figure>
			</div>
		</div>
		<div id="collapseScore" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
			<div class="card card-body">
				<form method="POST" action="{{ route('dashboard.exam-score.index', $exam->id) }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="card card-body">
						<input name="score" type="file">
						<div class="text-center">
							<button class="btn btn-primary" type="submit">上傳成績檔</button>
						</div>
					</div>
				</form>
				<div class="mb-3"></div>
				<figure class="figure text-center">
					<img src="{{ asset('img/score-1.PNG') }}" class="figure-img img-fluid rounded" alt="">
					<figcaption class="figure-caption text-center">1. 選擇
						<span class="badge badge-primary">Reports</span></figcaption>
					<figcaption class="figure-caption text-center">2. 下拉清單選項選擇
						<span class="badge badge-info">Standings Web Pages</span> 按鈕
					</figcaption>
					<figcaption class="figure-caption text-center">3. 點擊
						<span class="badge badge-info">Generate Summary</span> 按鈕
					</figcaption>
				</figure>
				<figure class="figure text-center">
					<img src="{{ asset('img/score-2.PNG') }}" class="figure-img img-fluid rounded" alt="">
					<figcaption class="figure-caption text-center">4. 開啟 PC<sup>2</sup>下的,
						<span class="badge badge-primary">reports</span> 資料夾
					</figcaption>
					<figcaption class="figure-caption text-center">5. 將
						<span class="badge badge-primary">report.Standings_Web_Pages.pc2export(...).html</span> 檔案上傳至系統
					</figcaption>
				</figure>
			</div>
		</div>
	</div>

	<table id="exam" class="table table-striped">
		<thead>
		<tr>
			<th>#</th>
			<th>學號</th>
			<th>名字</th>
			<th>英文名字</th>
			<th>報考語言</th>
			<th>完成題數</th>
			<th>報名時間</th>
		</tr>
		</thead>
	</table>

@endsection

@push('js')
	<script src="{{ asset('js/Chart.js') }}"></script>
@endpush

@push('script')
	<script>
		$(function () {
			$('#exam').DataTable({
				processing: true,
				serverSide: true,
				stateSave: true,
				ajax: '{{ route('api.signup',$exam->id) }}',
				columns: [
					{data: 'index'},
					{data: 'student_id'},
					{data: 'name_zh_tw'},
					{data: 'name_en'},
					{data: 'lang'},
					{data: 'solve_problem'},
					{data: 'created_at'},
				],
				"language": {
					"url": "{{ route('js.datatable') }}"
				}
			});

			$('#update-threshold').click(function () {
				axios.post('{{ route('dashboard.exam.threshold',$exam->id) }}', {pass_threshold: $('#pass_threshold').val()})
					.then((response) => {
						if (response.status === 200) {
							if (response.data.success) {
								swal(response.data.message, '', response.data.status);
							} else {
								swal('發生錯誤', response.data.message, 'warning');
							}
						} else if (response.status === 422) {
							swal('Error!', response.data.errors.solve_problem, 'warning');
						}
					})
					.catch((error) => {
						swal('發生錯誤', 'error', 'warning');
					});
			});

			new Chart($("#myChart"), {
				"type": "pie",
				"data": {
					"labels": @json(array_keys($data->toArray())),
					"datasets": [{
						"label":"解題題數",
						"data": @json(array_values($data->toArray())),
						"fill": false,
						"backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)", "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)", "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"],
						"borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)", "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"],
						"borderWidth": 1
					}]
				},
				"options": {
					responsive: true,
					title: {
						display: true,
						text: '解題題數'
					}
				}
			});

		});
	</script>
@endpush