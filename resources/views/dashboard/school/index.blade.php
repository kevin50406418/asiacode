@extends('layouts.app')

@section('page-title','學校管理')

@section('content')
	<table id="users" class="table table-striped table-bordered">
		<thead>
		<tr>
			<th>學校名稱</th>
			<th>是否開放報名</th>
			<th>操作</th>
		</tr>
		</thead>
	</table>
@endsection

@push('script')
	<script>
		$(function () {
			$('#users').DataTable({
				processing: true,
				serverSide: true,
				ajax: '{{ route('api.school.all') }}',
				columns: [
					{data: 'name'},
					{data: 'open'},
					{data: 'action', name: 'action', orderable: false, searchable: false}
				],
				language: {
					"url": "{{ route('js.datatable') }}"
				}
			});
		});
	</script>
@endpush