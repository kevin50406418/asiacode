@extends('layouts.app')

@section('page-title','成績說明設定：'.$exam->name)

@section('content')
	<h2 class="display-4">{{ $exam->name }}</h2>
	<hr class="my-4">
	<div>
		<i class="fas fa-map-marker-alt fa-fw"></i> 考試地點：{{ $exam->time_end }}
	</div>
	<div class="mb-3">
		<i class="fas fa-calendar-alt fa-fw"></i> 考試時間：{{ $exam->time_start->format('Y-m-d H:i') }} ~ {{ $exam->time_end->format('Y-m-d H:i') }}
	</div>
	<div class="mt-3 mb-3">
		<i class="fas fa-calendar-check"></i> 報名時間：{{ $exam->signup_start->format('Y-m-d H:i') }} ~ {{ $exam->signup_end->format('Y-m-d H:i') }}
	</div>
	<div class="card">
		<div class="card-body">
			{!! $exam->desc !!}
		</div>
	</div>

	@if( is_null($exam->pass_threshold) )
		<div class="alert alert-info">
			提示: 尚未設定通過門檻
		</div>
	@endif

	<div class="mt-3 mb-3">
		<a href="{{ route('dashboard.exam.show', $exam->id) }}" class="btn btn-outline-primary" role="button">成績匯入</a>
		<a href="{{ route('dashboard.exam-export.list', $exam->id) }}" class="btn btn-success">匯出名單</a>
		<a href="{{ route('dashboard.grade-ability.edit', $exam->id) }}" class="btn btn-outline-info">成績說明設定</a>
	</div>

	<hr>

	<form method="POST" action="{{ route('dashboard.grade-ability.update', $exam->id) }}">
		{{ csrf_field() }}
		{{ method_field('PUT') }}
		<div class="form-group">
			<label for="slug">證照階級代碼*</label>
			<input id="slug" type="text" size="5" class="form-control {{ $errors->has('slug') ? 'is-invalid' : '' }}" name="slug" value="{{ old('slug',optional($grade)->slug) }}" required autofocus>
			@if ($errors->has('slug'))
				<div class="invalid-feedback"> {{ $errors->first('slug') }} </div>
			@endif
		</div>
		<div class="form-group">
			<label for="cert_title_en">證照英文標題*</label>
			<input id="cert_title_en" type="text" class="form-control {{ $errors->has('cert_title_en') ? 'is-invalid' : '' }}" name="cert_title_en" value="{{ old('cert_title_en',optional($grade)->cert_title_en) }}" required>
			@if ($errors->has('cert_title_en'))
				<div class="invalid-feedback"> {{ $errors->first('cert_title_en') }} </div>
			@endif
			<p class="text-info">取代代碼: <code>{code}</code> 系統可以轉換為報名者報名程式語言 </p>
		</div>
		<div class="form-group">
			<label for="cert_title_zh_tw">證照中文標題*</label>
			<input id="cert_title_zh_tw" type="text" class="form-control {{ $errors->has('cert_title_zh_tw') ? 'is-invalid' : '' }}" name="cert_title_zh_tw" value="{{ old('cert_title_zh_tw',optional($grade)->cert_title_zh_tw) }}" required>
			@if ($errors->has('cert_title_zh_tw'))
				<div class="invalid-feedback"> {{ $errors->first('cert_title_zh_tw') }} </div>
			@endif
			<p class="text-info">取代代碼: <code>{code}</code> 系統可以轉換為報名者報名程式語言 </p>
		</div>
		<div class="form-group">
			<label for="desc">能力指標說明*</label>
			<textarea id="desc" name="desc" class="form-control {{ $errors->has('desc') ? 'is-invalid' : '' }}">{{ old('desc',optional($grade)->desc) }}</textarea>
			@if ($errors->has('desc'))
				<div class="invalid-feedback"> {{ $errors->first('desc') }} </div>
			@endif
		</div>

		<button type="submit" class="btn btn-info">儲存</button>
	</form>

@endsection

@push('js')

@endpush

@push('js')
	<script src="//cdn.ckeditor.com/4.8.0/full/ckeditor.js"></script>
	<script src="//cdn.ckeditor.com/4.8.0/full/adapters/jquery.js"></script>
@endpush

@push('script')
	<script>
		$(function () {
			$('textarea#desc').ckeditor({
				toolbarGroups: [
					{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
					{ name: 'colors', groups: [ 'colors' ] },
					{ name: 'links', groups: [ 'links' ] },
					{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
				],
				enterMode: CKEDITOR.ENTER_BR,
				removeButtons: 'ShowBlocks,Maximize,About,Styles,Format,Font,FontSize,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,BidiLtr,BidiRtl,Language,Blockquote,CreateDiv,CopyFormatting,Source,Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Replace,Find,SelectAll,Scayt,Anchor',
			});
		});
	</script>
@endpush
