@extends('layouts.app')

@section('page-title','使用者資料編輯')

@section('content')
	@includeWhen($errors->any(), 'component.alert-validation', ['errors' => $errors])
	<form method="POST" action="{{ route('dashboard.user.update', $user->id) }}">
		{{ csrf_field() }}
		{{ method_field('PUT') }}
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="username">帳號*</label>
				<input id="username" type="text" class="form-control-plaintext {{ $errors->has('username') ? 'is-invalid' : '' }}" name="username" value="{{ old('username', $user->username) }}">
				@if ($errors->has('username'))
					<div class="invalid-feedback"> {{ $errors->first('username') }} </div>
				@endif
			</div>

			<div class="form-group col-md-6">
				<label for="sex">性別*</label>
				<div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" class="custom-control-input" id="sex_0" name="sex" required value="0"{{ old('sex', $user->sex) == 0 ? 'checked' : '' }}>
						<label class="custom-control-label" for="sex_0">女</label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" class="custom-control-input" id="sex_1" name="sex" required value="1"{{ old('sex', $user->sex) == 1 ? 'checked' : '' }}>
						<label class="custom-control-label" for="sex_1">男</label>
					</div>
				</div>
				@if ($errors->has('last_name_en'))
					<div class="invalid-feedback"> {{ $errors->first('last_name_en') }} </div>
				@endif
			</div>

			<div class="form-group col-md-6">
				<label for="student_id">學號*</label>
				<input id="student_id" type="text" class="form-control {{ $errors->has('student_id') ? 'is-invalid' : '' }}" name="student_id" value="{{ old('student_id', $user->student_id) }}" v-model="student_id">
				@if ($errors->has('student_id'))
					<div class="invalid-feedback"> {{ $errors->first('student_id') }} </div>
				@endif
			</div>

			<div class="form-group col-md-6">
				<label for="name_zh_tw">姓名*</label>
				<input id="name_zh_tw" type="text" class="form-control {{ $errors->has('name_zh_tw') ? 'is-invalid' : '' }}" name="name_zh_tw" value="{{ old('name_zh_tw', $user->name_zh_tw) }}">
				@if ($errors->has('name_zh_tw'))
					<div class="invalid-feedback"> {{ $errors->first('name_zh_tw') }} </div>
				@endif
			</div>

			<div class="form-group col-md-6">
				<label for="first_name_en">英文名*</label>
				<input id="first_name_en" type="text" class="form-control {{ $errors->has('first_name_en') ? 'is-invalid' : '' }}" name="first_name_en" value="{{ old('first_name_en', $user->first_name_en) }}">
				@if ($errors->has('first_name_en'))
					<div class="invalid-feedback"> {{ $errors->first('first_name_en') }} </div>
				@endif
			</div>
			<div class="form-group col-md-6">
				<label for="last_name_en">英文姓*</label>
				<input id="last_name_en" type="text" class="form-control {{ $errors->has('last_name_en') ? 'is-invalid' : '' }}" name="last_name_en" value="{{ old('last_name_en', $user->last_name_en) }}">
				@if ($errors->has('last_name_en'))
					<div class="invalid-feedback"> {{ $errors->first('last_name_en') }} </div>
				@endif
			</div>

			<div class="form-group col-md-4">
				<label for="school_id">學校*</label>
				<select class="form-control {{ $errors->has('school_id') ? 'is-invalid' : '' }}" name="school_id" id="school_id" v-model="school_id" v-on:change="getDepts">
					<option value="">請先選擇學校</option>
					<option v-for="school in schools" v-bind:value="school.id">
						@{{ school.name }}
					</option>
				</select>
				@if ($errors->has('school_id'))
					<div class="invalid-feedback"> {{ $errors->first('school_id') }} </div>
				@endif
			</div>
			<div class="form-group col-md-4">
				<label for="dept_id">學系*</label>
				<select class="form-control {{ $errors->has('dept_id') ? 'is-invalid' : '' }}" name="dept_id" id="dept_id" v-model="dept_id">
					<option value="">請先選擇學系</option>
					<option v-for="dept in depts" v-bind:value="dept.id">
						@{{ dept.name }}
					</option>
				</select>
				@if ($errors->has('dept_id'))
					<div class="invalid-feedback"> {{ $errors->first('dept_id') }} </div>
				@endif
			</div>
			<div class="form-group col-md-4">
				<label for="edu_level">學制*</label>
				<div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" class="custom-control-input" id="edu_level_b" name="edu_level" required value="B"{{ old('edu_level', $user->edu_level) == 'B' ? 'checked' : '' }}>
						<label class="custom-control-label" for="edu_level_b">大學部</label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" class="custom-control-input" id="edu_level_m" name="edu_level" required value="M"{{ old('edu_level', $user->edu_level) == 'M' ? 'checked' : '' }}>
						<label class="custom-control-label" for="edu_level_m">碩士班</label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" class="custom-control-input" id="edu_level_d" name="edu_level" required value="D"{{ old('edu_level', $user->edu_level) == 'D' ? 'checked' : '' }}>
						<label class="custom-control-label" for="edu_level_d">博士班</label>
					</div>
				</div>
				@if ($errors->has('edu_level'))
					<div class="invalid-feedback"> {{ $errors->first('edu_level') }} </div>
				@endif
			</div>

			<div class="form-group col-md-9">
				<label for="email">信箱*</label>
				<input id="email" type="text" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" value="{{ old('email', $user->email) }}">
				@if ($errors->has('email'))
					<div class="invalid-feedback"> {{ $errors->first('email') }} </div>
				@endif
			</div>

			<div class="form-group col-md-3">
				<div>
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input {{ $errors->has('verified') ? 'is-invalid' : '' }}" id="verified" name="verified" value="1" {{ old('verified', $user->verified)? 'checked' : '' }}>
						<label class="custom-control-label" for="verified">信箱認證</label>
					</div>
				</div>
				@if ($errors->has('verified'))
					<div class="invalid-feedback"> {{ $errors->first('verified') }} </div>
				@endif
			</div>
		</div>

		<button type="submit" class="btn btn-primary">修改</button>
	</form>
@endsection

@push('script')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				student_id: '{{ old('student_id', $user->student_id) }}',
				student_email: '',
				schools: @json($schools),
				school_id: '{{ old('school_id', $user->school_id) }}',
				dept_id: '{{ old('dept_id', $user->dept_id) }}',
				depts: []
			},
			mounted: function () {
				this.getDepts();
			},
			methods: {
				getDepts: function () {
					const self = this;
					if (this.school_id !== '') {
						this.student_email = JSON.parse(JSON.stringify(this.schools))[this.school_id].student_email;
					}
					axios.get('{{ route('api.dept') }}?school_id=' + self.school_id).then(function (response) {
						if (response.status === 200) {
							self.depts = response.data;
						}
					});
				},
			}
		});

	</script>
@endpush
