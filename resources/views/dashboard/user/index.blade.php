@extends('layouts.app')

@section('page-title','使用者管理')

@section('content')
	<table id="users" class="table table-striped table-bordered">
		<thead>
		<tr>
			<th>學號</th>
			<th>帳號</th>
			<th>姓名</th>
			<th>信箱認證</th>
			<th>操作</th>
		</tr>
		</thead>
	</table>
@endsection

@push('script')
	<script>
		$(function () {
			$('#users').DataTable({
				processing: true,
				serverSide: true,
				ajax: '{{ route('api.user.all') }}',
				columns: [
					{data: 'student_id'},
					{data: 'username'},
					{data: 'name_zh_tw'},
					{data: 'verified'},
					{data: 'action', name: 'action', orderable: false, searchable: false}
				],
				language: {
					"url": "{{ route('js.datatable') }}"
				}
			});
		});
	</script>
@endpush