@extends('layouts.app')

@section('page-title','題庫管理：無測資資料')

@section('content')
	<div class="ex-page-content text-center">
		<h1 class="">無測資資料</h1>
		<h3 class="">抱歉!無測資資料，若要增加請透過 Github 增加</h3><br>

		<a class="btn btn-info mb-5 waves-effect waves-light" href="{{ route('home.index') }}">Back to Home</a>
	</div>
@endsection