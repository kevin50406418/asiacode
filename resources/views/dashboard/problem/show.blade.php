@extends('layouts.app')

@section('page-title','題庫： Problem '. $problem->problem_id.'.'.$problem->title)

@section('page-breadcrumb')
	<ol class="breadcrumb hide-phone p-0 m-0">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">首頁</a></li>
		<li class="breadcrumb-item"><a href="{{ route('dashboard.problem.index') }}">題庫管理</a></li>
		<li class="breadcrumb-item active">題庫： Problem {{ $problem->problem_id }}.{{ $problem->title }}</li>
	</ol>
@endsection

@section('content')
	<h2 class="mt-4 text-center">Problem {{ $problem->problem_id }}.{{ $problem->title }}</h2>
	<div class="text-center">（時間限制：{{ $problem->time_limit }} 秒）</div>
	@if( !empty($problem->source) )
		<div class="mt-4">
			題目來源：{{ $problem->source }}
		</div>
	@endif
	<h3 class="mt-4">問題描述：</h3>
	<div class="mt-4 mb-4">
		{!! $problem->description !!}
	</div>

	@if( !empty($problem->hint) )
		<div class="alert alert-info">
			{{ $problem->hint }}
		</div>
	@endif

	<h3 class="mt-4">輸入說明：</h3>
	<div class="mt-4 mb-4">
		{!! $problem->input !!}
	</div>

	<h3 class="mt-4">輸出說明：</h3>
	<div class="mt-4 mb-4">
		{!! $problem->output !!}
	</div>

	<h3 class="mt-4">範例：</h3>
	@if( $problem->samples )
		<table class="table table-bordered">
			<thead>
			<tr>
				<th>Sample Input:</th>
				<th>Sample Output:</th>
			</tr>
			</thead>
			<tbody>
			@foreach($problem->samples as $sample)
				<tr>
					<td><pre>{{ $sample->sample_input }}</pre></td>
					<td><pre>{{ $sample->sample_output }}</pre></td>
				</tr>
			@endforeach
			</tbody>
		</table>
	@endif

	<h3 class="mt-4" id="testcase">測試資料：</h3>
	@if( $testcases )
		<table class="table table-bordered">
			<thead>
			<tr>
				<th>Sample Input:</th>
				<th>Sample Output:</th>
			</tr>
			</thead>
			<tbody>
			@foreach( range(1, (count($testcases)/2) ) as $key)
				<tr>
					<td>
						<a href="{{ array_get($testcases,$key.'.in','#') }}">{{ $key.'.in' }}</a>
					</td>
					<td>
						<a href="{{ array_get($testcases,$key.'.out','#') }}">{{ $key.'.out' }}</a>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	@else
		<div class="alert alert-danger">抱歉!無測資資料，若要增加請透過 Github 增加</div>
	@endif

@endsection
