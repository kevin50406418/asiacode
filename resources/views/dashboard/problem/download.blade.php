@extends('layouts.app')

@section('page-title','考試管理')

@section('content')


	<table id="exam" class="table table-striped table-bordered">
		<thead>
		<tr>
			<th>輸入</th>
			<th>輸出</th>
		</tr>
		</thead>
		<tbody>
		@foreach($exams as $exam)
			<tr>
				<td>
					{{ $exam->name }}
				</td>
				<td>
					{{ $exam->signup_start->format('Y-m-d H:i') }} ~ {{ $exam->signup_end->format('Y-m-d H:i') }}
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
@endsection

@push('script')

@endpush