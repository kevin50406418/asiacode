@extends('layouts.app')

@section('page-title','考試題目設定：'.$exam->name)

@section('content')
	<h2 class="display-4">{{ $exam->name }}</h2>
	<hr class="my-4">
	<div>
		<i class="fas fa-map-marker-alt fa-fw"></i> 考試地點：{{ $exam->time_end }}
	</div>
	<div class="mb-3">
		<i class="fas fa-calendar-alt fa-fw"></i> 考試時間：{{ $exam->time_start->format('Y-m-d H:i') }} ~ {{ $exam->time_end->format('Y-m-d H:i') }}
	</div>
	<div class="mt-3 mb-3">
		<i class="fas fa-calendar-check"></i> 報名時間：{{ $exam->signup_start->format('Y-m-d H:i') }} ~ {{ $exam->signup_end->format('Y-m-d H:i') }}
	</div>
	<div class="card">
		<div class="card-body">
			{!! $exam->desc !!}
		</div>
	</div>

	<form action="{{ route('dashboard.exam-problem.update',$exam->id) }}" method="post">
		{{ csrf_field() }}
		{{ method_field('PUT') }}
		<div class="mt-2 mb-2">
			<button class="btn btn-success" type="button" @click="addRow">新增</button>
		</div>
		<table id="problem" class="table table-striped">
			<thead>
			<tr>
				<th>題目</th>
				<th>操作</th>
			</tr>
			</thead>
			<tbody>
			@foreach($exam_problems as $exam_problem)
				<tr v-show="show[{{ $exam_problem->problem_id }}]">
					<td>
						<a target="_blank" href="{{ route('problem.show',$exam_problem->problem_id ) }}">{{ $exam_problem->problem_id }}.{{ $exam_problem->problem->title }}</a>
					</td>
					<td>
						<a class="btn btn-info btn-xs" href="{{ route('dashboard.problem.show',$exam_problem->problem_id) }}#testcase">下載測資</a>
						<button class="btn btn-danger btn-xs" type="button" @click="delete_problem({{ $exam_problem->problem_id }});">刪除</button>
					</td>
				</tr>
			@endforeach
			<tr v-for="(row, index) in rows">
				<td>
					<select class="form-control" :name="'problem_id[' + index + ']'" v-model="row.problem_id">
						<option value="">請選擇題目</option>
						@foreach ($problems as $key => $problem)
							<option value="{{ $problem->problem_id }}">{{ $problem->problem_id }}.{{ $problem->title }}</option>
						@endforeach
					</select>
				</td>
				<td>
					<button class="btn btn-danger btn-xs" type="button" @click="removeElement(row);">刪除</button>
				</td>
			</tr>
			</tbody>
		</table>
		<div class="text-center">
			<button type="submit" class="btn btn-primary">設定題目</button>
		</div>
	</form>
@endsection

@push('script')
	<script>
		new Vue({
			el: '#app',
			data: {
				rows: [],
				show: @json($show)
			},
			methods: {
				addRow: function () {
					this.rows.push({
						problem_id: "",
					});
				},
				removeElement: function (row) {
					const index = this.rows.indexOf(row);
					this.rows.splice(index, 1);
				},
				delete_problem: function (problem_id) {
					const self = this;
					axios.delete('{{ route('dashboard.exam-problem.destroy',$exam->id) }}?problem_id=' + problem_id, {pass_threshold: $('#pass_threshold').val()})
						.then((response) => {
							if (response.status === 200) {
								if (response.data.success) {
									self.show[problem_id] = false;
									swal(response.data.message, '', response.data.status);
								} else {
									swal('發生錯誤', response.data.message, 'warning');
								}
							}
						})
						.catch((error) => {
							swal('發生錯誤', 'error', 'warning');
						});
				}
			}
		});
	</script>
@endpush