@extends('layouts.app')

@section('page-title','成績匯入：'.$exam->name)

@section('content')
	<h2 class="display-4">{{ $exam->name }}</h2>
	<hr class="my-4">
	<div>
		<i class="fas fa-map-marker-alt fa-fw"></i> 考試地點：{{ $exam->time_end }}
	</div>
	<div class="mb-3">
		<i class="fas fa-calendar-alt fa-fw"></i> 考試時間：{{ $exam->time_start->format('Y-m-d H:i') }} ~ {{ $exam->time_end->format('Y-m-d H:i') }}
	</div>
	<div class="mt-3 mb-3">
		<i class="fas fa-calendar-check"></i> 報名時間：{{ $exam->signup_start->format('Y-m-d H:i') }} ~ {{ $exam->signup_end->format('Y-m-d H:i') }}
	</div>
	<div class="card">
		<div class="card-body">
			{!! $exam->desc !!}
		</div>
	</div>

	<form action="{{ route('dashboard.exam-score.store',$exam->id) }}" method="post">
		{{ csrf_field() }}
		<table id="exam" class="table table-striped">
			<thead>
			<tr>
				<th>學號</th>
				<th>名字</th>
				<th>報考語言</th>
				<th>PC<sup>2</sup>帳號</th>
				<th>完成題數</th>
			</tr>
			</thead>
			<tbody>
			@foreach($signups as $signup)
				<tr>
					<td>
						{{ $signup->student_id }}
					</td>
					<td>
						{{ $signup->name_zh_tw }}
					</td>
					<td>
						{{ $langs[ $signup->lang_id ]->lang->lang }}
					</td>
					<td>
						team{{ $signup->pc2_account }}
					</td>
					<td>
						<input type="text" name="solve_problem[{{ $signup->pc2_account }}]" value="{{ old('solve_problem.'.$signup->pc2_account, array_get($lists,$signup->pc2_account,0)) }}">
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		<div class="text-center">
			<button type="submit" class="btn btn-primary">輸入成績</button>
		</div>
	</form>
@endsection

