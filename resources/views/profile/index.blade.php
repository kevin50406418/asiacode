@extends('layouts.app')

@section('page-title','個人資料')

@section('content')
	<div class="alert alert-info">
		以下個人資料若有誤，請與主辦方更改資料
	</div>
	<div class="form-row">
		<div class="form-group col-md-6">
			<label for="name_zh_tw">姓名*</label>
			<input id="name_zh_tw" type="text" readonly class="form-control-plaintext" name="name_zh_tw" value="{{ $user->name_zh_tw }}">
		</div>
		<div class="form-group col-md-6">
			<label for="sex">性別*</label>
			<div>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" class="custom-control-input" id="sex_0" name="sex" disabled value="0"{{ $user->sex == 0 ? 'checked' : '' }}>
					<label class="custom-control-label" for="sex_0">女</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" class="custom-control-input" id="sex_1" name="sex" disabled value="1"{{ $user->sex == 1 ? 'checked' : '' }}>
					<label class="custom-control-label" for="sex_1">男</label>
				</div>
			</div>
		</div>

		<div class="form-group col-md-3">
			<label for="first_name_en">英文名*</label>
			<input id="first_name_en" type="text" readonly class="form-control-plaintext" name="first_name_en" value="{{ $user->first_name_en }}">
			@if ($errors->has('first_name_en'))
				<div class="invalid-feedback"> {{ $errors->first('first_name_en') }} </div>
			@endif
		</div>
		<div class="form-group col-md-3">
			<label for="last_name_en">英文姓*</label>
			<input id="last_name_en" type="text" readonly class="form-control-plaintext" name="last_name_en" value="{{  $user->last_name_en }}">
			@if ($errors->has('last_name_en'))
				<div class="invalid-feedback"> {{ $errors->first('last_name_en') }} </div>
			@endif
		</div>

		<div class="form-group col-md-6">
			<label for="name_zh_tw">信箱*</label>
			<input id="email" type="text" readonly class="form-control-plaintext" name="email" value="{{ $user->email }}">
		</div>

		<div class="form-group col-md-3">
			<label for="school_id">學校*</label>
			<input id="school_id" type="text" readonly class="form-control-plaintext" name="school_id" value="{{ $schools[$user->school_id]->name }}">
		</div>
		<div class="form-group col-md-3">
			<label for="dept_id">學系*</label>
			<input id="dept_id" type="text" readonly class="form-control-plaintext" name="dept_id" value="{{ $depts[$user->dept_id]->name }}">
		</div>
		<div class="form-group col-md-3">
			<label for="edu_level">學制*</label>
			@switch( $user->edu_level )
				@case('B')
				<input id="edu_level" type="text" readonly class="form-control-plaintext" name="edu_level" value="大學部">
				@break
				@case('M')
				<input id="edu_level" type="text" readonly class="form-control-plaintext" name="edu_level" value="碩士班">
				@break
				@case('D')
				<input id="edu_level" type="text" readonly class="form-control-plaintext" name="edu_level" value="博士班">
				@break
				@default
				-
			@endswitch
		</div>
		<div class="form-group col-md-3">
			<label for="student_id">學號*</label>
			<input id="student_id" type="text" readonly class="form-control-plaintext" name="student_id" value="{{ $user->student_id }}">
		</div>
	</div>

	<h2>修改密碼</h2>
	<hr>
	<form method="POST" action="{{ route('profile.passwd') }}">
		{{ csrf_field() }}
		<div class="form-row">
			<div class="form-group col-md-12">
				<label for="old_password">舊密碼*</label>
				<input id="old_password" type="password" class="form-control {{ $errors->has('old_password') ? 'is-invalid' : '' }}" name="old_password">
				@if ($errors->has('old_password'))
					<div class="invalid-feedback"> {{ $errors->first('old_password') }} </div>
				@endif
			</div>
			<div class="form-group col-md-12">
				<label for="password">新密碼*</label>
				<input id="password" type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" name="password">
				@if ($errors->has('password'))
					<div class="invalid-feedback"> {{ $errors->first('password') }} </div>
				@endif
			</div>
			<div class="form-group col-md-12">
				<label for="password-confirm">確認新密碼*</label>
				<input id="password-confirm" type="password" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}" name="password_confirmation">
			</div>
		</div>
		<button type="submit" class="btn btn-primary">修改密碼</button>
	</form>
@endsection
