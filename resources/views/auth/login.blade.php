@extends('layouts.auth')

@section('content')
	<div class="wrapper-page">
		<div class="card">
			<div class="card-body">
				<h4 class="text-muted text-center font-18"><b>登入</b></h4>
				<div class="p-3">
					<form class="form-horizontal m-t-20" method="POST" action="{{ route('login') }}">
						{{ csrf_field() }}

						<div class="form-group row">
							<div class="col-12">
								<input id="username" type="text" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus placeholder="帳號">
								@if ($errors->has('username'))
									<div class="invalid-feedback"> {{ $errors->first('username') }} </div>
								@endif
							</div>
						</div>

						<div class="form-group row">
							<div class="col-12">
								<input id="password" type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" name="password" placeholder="密碼">
								@if ($errors->has('password'))
									<div class="invalid-feedback"> {{ $errors->first('password') }} </div>
								@endif
							</div>
						</div>

						<div class="form-group row">
							<div class="col-12">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
									<label class="custom-control-label" for="remember">記住我</label>
								</div>
							</div>
						</div>


						<div class="form-group text-center row m-t-20">
							<div class="col-12">
								<button class="btn btn-info btn-block waves-effect waves-light" type="submit">登入</button>
							</div>
						</div>

						<div class="form-group m-t-10 mb-0 row">
							{{--<div class="col-sm-7 m-t-20">--}}
								{{--<a href="pages-recoverpw.html" class="text-muted"><i class="mdi mdi-lock    "></i> Forgot your password?</a>--}}
							{{--</div>--}}
							<div class="col-sm-5 m-t-20">
								<a href="{{ route('register') }}" class="text-muted"><i class="fas fa-user-circle"></i> 建立一個帳號</a>
							</div>
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>
@endsection
