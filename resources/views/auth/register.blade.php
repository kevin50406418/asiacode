@extends('layouts.app')

@section('page-title', '帳號註冊')

@section('page-breadcrumb')
	<ol class="breadcrumb hide-phone p-0 m-0">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">首頁</a></li>
		<li class="breadcrumb-item active">帳號註冊</li>
	</ol>
@endsection

@section('content')
	<div class="p-3">
		@includeWhen($errors->any(), 'component.alert-validation', ['errors' => $errors])
		<form class="form-horizontal m-t-20" method="POST" action="{{ route('register') }}">
			{{ csrf_field() }}
			<div class="form-row">
				<div class="form-group col-md-5">
					<label for="username">帳號*</label>
					<input id="username" type="text" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>
					@if ($errors->has('username'))
						<div class="invalid-feedback"> {{ $errors->first('username') }} </div>
					@endif
				</div>

				<div class="form-group col-md-4">
					<label for="id_number">身分證*</label>
					<input id="id_number" type="text" class="form-control {{ $errors->has('id_number') ? 'is-invalid' : '' }}" name="id_number" value="{{ old('id_number') }}" required>
					@if ($errors->has('id_number'))
						<div class="invalid-feedback"> {{ $errors->first('id_number') }} </div>
					@endif
				</div>

				<div class="form-group col-md-3">
					<label for="sex">性別*</label>
					<div>
						<div class="custom-control custom-radio custom-control-inline">
							<input type="radio" class="custom-control-input" id="sex_0" name="sex" required value="0"{{ old('sex') == 0 ? 'checked' : '' }}>
							<label class="custom-control-label" for="sex_0">女</label>
						</div>
						<div class="custom-control custom-radio custom-control-inline">
							<input type="radio" class="custom-control-input" id="sex_1" name="sex" required value="1"{{ old('sex') == 1 ? 'checked' : '' }}>
							<label class="custom-control-label" for="sex_1">男</label>
						</div>
					</div>
					@if ($errors->has('sex'))
						<div class="invalid-feedback"> {{ $errors->first('sex') }} </div>
					@endif
				</div>

				<div class="form-group col-md-6">
					<label for="password">密碼*</label>
					<input id="password" type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" name="password">
					@if ($errors->has('password'))
						<div class="invalid-feedback"> {{ $errors->first('password') }} </div>
					@endif
				</div>
				<div class="form-group col-md-6">
					<label for="password-confirm">確認密碼*</label>
					<input id="password-confirm" type="password" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}" name="password_confirmation">
				</div>

				<div class="form-group col-md-6">
					<label for="student_id">學號*</label>
					<input id="student_id" type="text" class="form-control {{ $errors->has('student_id') ? 'is-invalid' : '' }}" name="student_id" value="{{ old('student_id') }}" v-model="student_id">
					@if ($errors->has('student_id'))
						<div class="invalid-feedback"> {{ $errors->first('student_id') }} </div>
					@endif
				</div>

				<div class="form-group col-md-6">
					<label for="name_zh_tw">姓名*</label>
					<input id="name_zh_tw" type="text" class="form-control {{ $errors->has('name_zh_tw') ? 'is-invalid' : '' }}" name="name_zh_tw" value="{{ old('name_zh_tw') }}">
					@if ($errors->has('name_zh_tw'))
						<div class="invalid-feedback"> {{ $errors->first('name_zh_tw') }} </div>
					@endif
				</div>

				<div class="form-group col-md-6">
					<label for="first_name_en">英文名*</label>
					<input id="first_name_en" type="text" class="form-control {{ $errors->has('first_name_en') ? 'is-invalid' : '' }}" name="first_name_en" value="{{ old('first_name_en') }}">
					@if ($errors->has('first_name_en'))
						<div class="invalid-feedback"> {{ $errors->first('first_name_en') }} </div>
					@endif
				</div>
				<div class="form-group col-md-6">
					<label for="last_name_en">英文姓*</label>
					<input id="last_name_en" type="text" class="form-control {{ $errors->has('last_name_en') ? 'is-invalid' : '' }}" name="last_name_en" value="{{ old('last_name_en') }}">
					@if ($errors->has('last_name_en'))
						<div class="invalid-feedback"> {{ $errors->first('last_name_en') }} </div>
					@endif
				</div>

				<div class="form-group col-md-4">
					<label for="school_id">學校*</label>
					<select class="form-control {{ $errors->has('school_id') ? 'is-invalid' : '' }}" name="school_id" id="school_id" v-model="school_id" v-on:change="getDepts">
						<option value="">請先選擇學校</option>
						<option v-for="school in schools" v-bind:value="school.id">
							@{{ school.name }}
						</option>
					</select>
					@if ($errors->has('school_id'))
						<div class="invalid-feedback"> {{ $errors->first('school_id') }} </div>
					@endif
				</div>
				<div class="form-group col-md-4">
					<label for="dept_id">學系*</label>
					<select class="form-control {{ $errors->has('dept_id') ? 'is-invalid' : '' }}" name="dept_id" id="dept_id">
						<option value="">請先選擇學系</option>
						<option v-for="dept in depts" v-bind:value="dept.id">
							@{{ dept.name }}
						</option>
					</select>
					@if ($errors->has('dept_id'))
						<div class="invalid-feedback"> {{ $errors->first('dept_id') }} </div>
					@endif
				</div>

				<div class="form-group col-md-4">
					<label for="edu_level">學制*</label>
					<div>
						<div class="custom-control custom-radio custom-control-inline">
							<input type="radio" class="custom-control-input" id="edu_level_b" name="edu_level" required value="B"{{ old('edu_level','B') == 'B' ? 'checked' : '' }}>
							<label class="custom-control-label" for="edu_level_b">大學部</label>
						</div>
						<div class="custom-control custom-radio custom-control-inline">
							<input type="radio" class="custom-control-input" id="edu_level_m" name="edu_level" required value="M"{{ old('edu_level','B') == 'M' ? 'checked' : '' }}>
							<label class="custom-control-label" for="edu_level_m">碩士班</label>
						</div>
						<div class="custom-control custom-radio custom-control-inline">
							<input type="radio" class="custom-control-input" id="edu_level_d" name="edu_level" required value="D"{{ old('edu_level','B') == 'D' ? 'checked' : '' }}>
							<label class="custom-control-label" for="edu_level_d">博士班</label>
						</div>
					</div>
					@if ($errors->has('edu_level'))
						<div class="invalid-feedback"> {{ $errors->first('edu_level') }} </div>
					@endif
				</div>

				<div class="form-group col-md-12">
					<label for="email">信箱*</label>
					{{--<div>@{{ student_id }}@@{{ student_email }}</div>--}}
					<input id="email" type="text" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" value="{{ old('email') }}">
					@if ($errors->has('email'))
						<div class="invalid-feedback"> {{ $errors->first('email') }} </div>
					@endif
				</div>
			</div>

			<div class="text-danger">* 學校、學系資訊若位於列表出現，請與我們聯絡</div>
			{{--<div class="text-info mb-2">** 若要開設學系、教師帳號，請與我們聯絡</div>--}}

			<div class="form-group text-center row m-t-20">
				<div class="col-12">
					<button class="btn btn-info btn-block waves-effect waves-light" type="submit">註冊</button>
				</div>
			</div>

			<div class="form-group m-t-10 mb-0 row">
				<div class="col-12 m-t-20 text-center">
					<a href="{{ route('login') }}" class="text-muted">已經有帳號?</a>
				</div>
			</div>
		</form>
	</div>

@endsection

@push('script')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				student_id: '{{ old('student_id') }}',
				student_email: '',
				schools: @json($schools),
				school_id: '{{ old('school_id') }}',
				depts: []
			},
			mounted: function () {
				this.getDepts();
			},
			methods: {
				getDepts: function () {
					const self = this;
					if (this.school_id !== '') {
						this.student_email = JSON.parse(JSON.stringify(this.schools))[this.school_id].student_email;
					}
					axios.get('{{ route('api.dept') }}?school_id=' + self.school_id).then(function (response) {
						if (response.status === 200) {
							self.depts = response.data;
						}
					});
				},
			}
		});

	</script>
@endpush
