{
	"sProcessing":   "{{ trans('datatable.sProcessing') }}",
	{{--"sProcessing":   "<div id='datatable-loader'></div>",--}}

	"sLengthMenu":   "{{ trans('datatable.sLengthMenu') }}",
	"sZeroRecords":  "{{ trans('datatable.sZeroRecords') }}",
	"sInfo":         "{{ trans('datatable.sInfo') }}",
	"sInfoEmpty":    "{{ trans('datatable.sInfoEmpty') }}",
	"sInfoFiltered": "{{ trans('datatable.sInfoFiltered') }}",
	"sInfoPostFix":  "{{ trans('datatable.sInfoPostFix') }}",
	"sSearch":       "{{ trans('datatable.sSearch') }}",
	"sUrl":          "",
	"oPaginate": {
		"sFirst":    "{{ trans('datatable.oPaginate.sFirst') }}",
		"sPrevious": "{{ trans('datatable.oPaginate.sPrevious') }}",
		"sNext":     "{{ trans('datatable.oPaginate.sNext') }}",
		"sLast":     "{{ trans('datatable.oPaginate.sLast') }}"
	}
}
