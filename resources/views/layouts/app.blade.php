<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('page-title') - {{ config('asiacode.site_name') }}</title>
	<link href="{{ asset('css/style.css') }}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/earlyaccess/notosanstc.css" rel="stylesheet">
	<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>
<body>
<div id="app">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div>
	@include('layouts.header')
	<div class="wrapper mt-3">
		<div class="container">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title-box">
						<div class="btn-group pull-right">
							@yield('page-breadcrumb')
						</div>
						<h4 class="page-title">@yield('page-title')</h4>
					</div>
				</div>
			</div>
			<div class="card m-b-30">
				<div class="card-body">
					@yield('content')
				</div>
			</div>
		</div>
	</div>
</div>
@guest
	<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog">
		<form method="POST" action="{{ route('login') }}">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">登入</h5>
					</div>
					<div class="modal-body">
						{{ csrf_field() }}
						<div class="form-group">
							<label for="username">帳號*</label>
							<input id="username" type="text" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>
							@if ($errors->has('username'))
								<div class="invalid-feedback"> {{ $errors->first('username') }} </div>
							@endif
						</div>
						<div class="form-group">
							<label for="password">密碼*</label>
							<input id="password" type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" name="password">
							@if ($errors->has('password'))
								<div class="invalid-feedback"> {{ $errors->first('password') }} </div>
							@endif
						</div>
						<div class="custom-control custom-checkbox mb-3">
							<input type="checkbox" class="custom-control-input" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
							<label class="custom-control-label" for="remember">記住我</label>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
						<a class="btn btn-info" href="{{ route('register') }}">註冊</a>
						<button type="submit" class="btn btn-primary">登入</button>
					</div>
				</div>
			</div>
		</form>
	</div>
@endguest
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/upcube.js') }}"></script>
@includeWhen(session()->has('msg'),'component.alert-success')
@includeWhen(session()->has('error_msg'),'component.alert-error')
@stack('js')
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
@stack('script')
</body>
</html>