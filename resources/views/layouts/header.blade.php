<!-- Navigation Bar-->
<header id="topnav">
	<div class="topbar-main">
		<div class="container">
			<!-- Logo container-->
			<div class="logo">
				<a href="{{ url('/') }}" class="logo">
					{{ config('asiacode.site_name') }}
				</a>
			</div>
			<!-- End Logo container-->
			<div class="menu-extras topbar-custom">
				<ul class="list-inline float-right mb-0">
					<!-- User-->
					@guest
						<li class="list-inline-item dropdown notification-list">
							<a class="nav-link waves-effect" href="{{ route('login') }}">
								登入
							</a>
						</li>
						<li class="list-inline-item dropdown notification-list">
							<a class="nav-link waves-effect" href="{{ route('register') }}">
								註冊
							</a>
						</li>
					@else
						<li class="list-inline-item dropdown notification-list">
							<a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
									aria-haspopup="false" aria-expanded="false">
								{{ Auth::user()->name_zh_tw }}
							</a>
							<div class="dropdown-menu dropdown-menu-right profile-dropdown ">
								<a class="dropdown-item" href="{{ route('profile.index') }}">個人資料</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="{{ route('logout') }}"> 登出</a>
							</div>
						</li>
					@endguest
					<li class="menu-item list-inline-item">
						<!-- Mobile menu toggle-->
						<a class="navbar-toggle nav-link">
							<div class="lines">
								<span></span>
								<span></span>
								<span></span>
							</div>
						</a>
						<!-- End mobile menu toggle-->
					</li>
				</ul>
			</div>
			<!-- end menu-extras -->
			<div class="clearfix"></div>
		</div> <!-- end container -->
	</div>
	<!-- end topbar-main -->
	<!-- MENU Start -->
	<div class="navbar-custom">
		<div class="container">
			<div id="navigation">
				<!-- Navigation Menu-->
				<ul class="navigation-menu">
					<li class="has-submenu">
						<a href="{{ route('home') }}">考試</a>
					</li>
					<li class="has-submenu">
						<a href="{{ route('problem.index') }}">題庫</a>
					</li>
					@role('admin')
					<li class="has-submenu">
						<a href="#">管理</a>
						<ul class="submenu">
							<li><a href="{{ route('dashboard.exam.index') }}">考試管理</a></li>
							<li><a href="{{ route('dashboard.user.index') }}">使用者管理</a></li>
						</ul>
					</li>
					@endrole
				</ul>
				<!-- End navigation menu -->
			</div> <!-- end #navigation -->
		</div> <!-- end container -->
	</div> <!-- end navbar-custom -->
</header>
<!-- End Navigation Bar-->