<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('asiacode.site_name') }}</title>
	<link href="{{ asset('css/style.css') }}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/earlyaccess/notosanstc.css" rel="stylesheet">
	<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>
<body>
<div id="app">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div>
	@include('layouts.header')
	<div class="wrapper mt-3">
		<div class="container">
			@yield('content')
		</div>
	</div>
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/upcube.js') }}"></script>
@includeWhen(session()->has('msg'),'component.alert-success')
@includeWhen(session()->has('error_msg'),'component.alert-error')
@stack('js')
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
@stack('script')
</body>
</html>