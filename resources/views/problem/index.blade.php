@extends('layouts.app')

@section('page-title', '題庫')

@section('page-breadcrumb')
	<ol class="breadcrumb hide-phone p-0 m-0">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">首頁</a></li>
		<li class="breadcrumb-item active">題庫</li>
	</ol>
@endsection

@section('content')
	<table id="exam" class="table table-striped table-bordered">
		<thead>
		<tr>
			<th>編號</th>
			<th>標題</th>
		</tr>
		</thead>
	</table>
@endsection

@push('script')
	<script>
		$(function () {
			$('#exam').DataTable({
				processing: true,
				serverSide: true,
				ajax: '{{ route('api.problem') }}',
				columns: [
					{data: 'problem_id'},
					{data: 'title'},
				],
				language: {
					"url": "{{ route('js.datatable') }}"
				}
			});
		});
	</script>
@endpush