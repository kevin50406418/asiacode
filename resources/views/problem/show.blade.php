@extends('layouts.app')

@section('page-title','題庫： Problem '. $problem->problem_id.'.'.$problem->title)

@section('page-breadcrumb')
	<ol class="breadcrumb hide-phone p-0 m-0">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">首頁</a></li>
		<li class="breadcrumb-item"><a href="{{ route('problem.index') }}">題庫</a></li>
		<li class="breadcrumb-item active">題庫： Problem {{ $problem->problem_id }}.{{ $problem->title }}</li>
	</ol>
@endsection

@section('content')
	<h2 class="mt-4 text-center">Problem {{ $problem->problem_id }}.{{ $problem->title }}</h2>
	<div class="text-center">（時間限制：{{ $problem->time_limit }} 秒）</div>
	@if( !empty($problem->source) )
		<div class="mt-4">
			題目來源：{{ $problem->source }}
		</div>
	@endif
	<h3 class="mt-4">問題描述：</h3>
	<div class="mt-4 mb-4">
		{!! $problem->description !!}
	</div>

	@if( !empty($problem->hint) )
		<div class="alert alert-info">
			{{ $problem->hint }}
		</div>
	@endif

	<h3 class="mt-4">輸入說明：</h3>
	<div class="mt-4 mb-4">
		{!! $problem->input !!}
	</div>

	<h3 class="mt-4">輸出說明：</h3>
	<div class="mt-4 mb-4">
		{!! $problem->output !!}
	</div>

	<h3 class="mt-4">範例：</h3>
	@if( $problem->samples )
		<table class="table table-bordered">
			<thead>
			<tr>
				<th>Sample Input:</th>
				<th>Sample Output:</th>
			</tr>
			</thead>
			<tbody>
			@foreach($problem->samples as $sample)
				<tr>
					<td><pre>{{ $sample->sample_input }}</pre></td>
					<td><pre>{{ $sample->sample_output }}</pre></td>
				</tr>
			@endforeach
			</tbody>
		</table>
	@endif

	{{--分類：--}}
	{{--<div>--}}
		{{--<a href="#" class="badge badge-primary">分類1</a>--}}
		{{--<a href="#" class="badge badge-primary">分類2</a>--}}
		{{--<a href="#" class="badge badge-primary">分類3</a>--}}
		{{--<a href="#" class="badge badge-primary">分類4</a>--}}
	{{--</div>--}}
@endsection

{{--@push('js')--}}
	{{--<script src="{{ asset('js/clipboard.js') }}"></script>--}}
{{--@endpush--}}

{{--@push('script')--}}
	{{--<script>--}}
		{{--$('pre').each(function (i) {--}}
			{{--$this = $(this);--}}
			{{--$button = $('<button class="pull-right btn btn-copy btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="複製到剪貼簿" data-clipboard-target="#code' + (i+1) + '">Copy</button>');--}}
			{{--$this.wrap('<div class="code-wrapper"><div/>');--}}
			{{--$this.attr('id','code'+(i+1));--}}
			{{--$wrapper = $this.parent();--}}
			{{--$button.appendTo($wrapper);--}}
		{{--});--}}
		{{--var copyCode = new Clipboard('button.btn-copy');--}}
		{{--copyCode.on('success', function (event) {--}}
			{{--event.clearSelection();--}}
			{{--event.trigger.className = 'btn-copied pull-right btn btn-copy btn-info btn-xs';--}}
			{{--event.trigger.textContent = 'Copied';--}}
			{{--event.trigger.nextSibling.children[1].innerHTML = '已複製到剪貼簿';--}}
			{{--window.setTimeout(function (e) {--}}
				{{--event.trigger.textContent = 'Copy';--}}
				{{--event.trigger.className = 'pull-right btn btn-copy btn-info btn-xs';--}}
				{{--event.trigger.nextSibling.className = 'tooltip top';--}}
			{{--}, 2000);--}}
		{{--});--}}
		{{--copyCode.on('error', function (event) {--}}
			{{--event.trigger.textContent = 'Press "Ctrl + C" to copy';--}}
			{{--window.setTimeout(function () {--}}
				{{--event.trigger.textContent = 'Copy';--}}
			{{--}, 2000);--}}
		{{--});--}}
	{{--</script>--}}
{{--@endpush--}}