@extends('layouts.app')

@section('page-title','考試：'. $exam->name)

@section('content')
	@include('exam.show-info')

	@role('student')
	<h3 class="mt-4">報名
		<small>{{ $exam->name }}</small>
	</h3>
	<hr class="my-4">
	<div class="form-row">
		<div class="form-group col-md-6">
			<label for="name_zh_tw">姓名*</label>
			<input id="name_zh_tw" type="text" readonly class="form-control-plaintext" name="name_zh_tw" value="{{ $signup->name_zh_tw }}">
		</div>

		<div class="form-group col-md-6">
			<label for="coding_lang">報考程式語言*</label>
			<select id="coding_lang" readonly class="form-control-plaintext" name="coding_lang">
				@foreach($langs as $lang)
					@if( $signup->lang_id == $lang->lang->id )
						<option value="{{ $lang->lang->id }}" {{ $signup->lang_id == $lang->lang->id ? 'selecetd' : '' }}>{{ $lang->lang->lang }}</option>
					@endif
				@endforeach
			</select>
			@if ($errors->has('coding_lang'))
				<div class="invalid-feedback"> {{ $errors->first('coding_lang') }} </div>
			@endif
		</div>

		<div class="form-group col-md-6">
			<label for="first_name_en">英文名*</label>
			<input id="first_name_en" type="text" readonly class="form-control-plaintext" name="first_name_en" value="{{ $signup->first_name_en }}">
			@if ($errors->has('first_name_en'))
				<div class="invalid-feedback"> {{ $errors->first('first_name_en') }} </div>
			@endif
		</div>
		<div class="form-group col-md-6">
			<label for="last_name_en">英文姓*</label>
			<input id="last_name_en" type="text" readonly class="form-control-plaintext" name="last_name_en" value="{{  $signup->last_name_en }}">
			@if ($errors->has('last_name_en'))
				<div class="invalid-feedback"> {{ $errors->first('last_name_en') }} </div>
			@endif
		</div>

		<div class="form-group col-md-3">
			<label for="school_id">學校*</label>
			<input id="school_id" type="text" readonly class="form-control-plaintext" name="school_id" value="{{ $schools[$signup->school_id]->name }}">
		</div>
		<div class="form-group col-md-3">
			<label for="dept_id">學系*</label>
			<input id="dept_id" type="text" readonly class="form-control-plaintext" name="dept_id" value="{{ $depts[$signup->dept_id]->name }}">
		</div>
		<div class="form-group col-md-3">
			<label for="edu_level">學制*</label>
			@switch(  $signup->edu_level )
				@case('B')
				<input id="edu_level" type="text" readonly class="form-control-plaintext" name="edu_level" value="大學部">
				@break
				@case('M')
				<input id="edu_level" type="text" readonly class="form-control-plaintext" name="edu_level" value="碩士班">
				@break
				@case('D')
				<input id="edu_level" type="text" readonly class="form-control-plaintext" name="edu_level" value="博士班">
				@break
				@default
				-
			@endswitch
		</div>
		<div class="form-group col-md-3">
			<label for="student_id">學號*</label>
			<input id="student_id" type="text" readonly class="form-control-plaintext" name="student_id" value="{{ $signup->student_id }}">
		</div>
	</div>

	@if( $exam->can_signup )
		<a class="btn btn-danger" href="{{ route('exam.cancel', $exam->id) }}">取消報名</a>
	@endif

	@if( now()->gt(Carbon\Carbon::parse($exam->publish_time)) && !is_null($signup->solve_problem) && !is_null($exam->pass_threshold) )
		<div>
			<h3 class="mt-4">考試成績</h3>
			<hr class="my-4">
			<table class="table table-bordered">
				<thead class="thead-dark">
				<tr>
					<th class="text-center">完成題數</th>
					<th class="text-center">通過證書</th>
				</tr>
				</thead>
				<thead>
				<tr>
					<td class="text-center">{{ $signup->solve_problem }}</td>
					<td class="text-center">
						@if( $signup->solve_problem >= $exam->pass_threshold )
							<span class="bg-success text-white p-2 rounded">通過</span>
						@else
							<span class="bg-danger text-white p-2 rounded">未通過</span>
						@endif
					</td>
				</tr>
				</thead>
			</table>
		</div>
	@endif
	@endrole
@endsection

