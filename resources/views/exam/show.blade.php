@extends('layouts.app')

@section('page-title','考試：'. $exam->name)

@section('page-breadcrumb')
	<ol class="breadcrumb hide-phone p-0 m-0">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">首頁</a></li>
		<li class="breadcrumb-item"><a href="{{ route('exam.index') }}">所有考試</a></li>
		<li class="breadcrumb-item active">考試：{{ $exam->name }}</li>
	</ol>
@endsection

@section('content')
	@include('exam.show-info')
	@if( $exam->can_signup )
		@auth
			@if( Auth::user()->verified )
				@role('student')
				<h3 class="mt-4">報名
					<small>{{ $exam->name }}</small>
				</h3>
				<hr class="my-4">
				<div class="alert alert-warning">
					<i class="fas fa-info-circle fa-fw"></i> 請核對以下資料是否正確，若錯誤請至個人資料更改
				</div>
				<form method="POST" action="{{ route('exam.store',[$exam->id]) }}">
					{{ csrf_field() }}
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="name_zh_tw">姓名*</label>
							<input id="name_zh_tw" type="text" readonly class="form-control-plaintext" name="name_zh_tw" value="{{Auth::user()->name_zh_tw }}">
						</div>

						<div class="form-group col-md-6">
							<label for="coding_lang">報考程式語言*</label>
							<select id="coding_lang" class="form-control {{ $errors->has('first_name_en') ? 'is-invalid' : '' }}" name="coding_lang">
								<option value="">請選擇報考的程式語言</option>
								@foreach($langs as $lang)
									<option value="{{ $lang->lang->id }}">{{ $lang->lang->lang }}</option>
								@endforeach
							</select>
							@if ($errors->has('coding_lang'))
								<div class="invalid-feedback"> {{ $errors->first('coding_lang') }} </div>
							@endif
						</div>

						<div class="form-group col-md-6">
							<label for="first_name_en">英文名*</label>
							<input id="first_name_en" type="text" readonly class="form-control-plaintext" name="first_name_en" value="{{ Auth::user()->first_name_en }}">
							@if ($errors->has('first_name_en'))
								<div class="invalid-feedback"> {{ $errors->first('first_name_en') }} </div>
							@endif
						</div>
						<div class="form-group col-md-6">
							<label for="last_name_en">英文姓*</label>
							<input id="last_name_en" type="text" readonly class="form-control-plaintext" name="last_name_en" value="{{ Auth::user()->last_name_en }}">
							@if ($errors->has('last_name_en'))
								<div class="invalid-feedback"> {{ $errors->first('last_name_en') }} </div>
							@endif
						</div>

						<div class="form-group col-md-4">
							<label for="school_id">學校*</label>
							<input id="school_id" type="text" readonly class="form-control-plaintext" name="school_id" value="{{ $schools[Auth::user()->school_id]->name }}">
						</div>
						<div class="form-group col-md-4">
							<label for="dept_id">學系*</label>
							<input id="dept_id" type="text" readonly class="form-control-plaintext" name="dept_id" value="{{ $depts[Auth::user()->dept_id]->name }}">
						</div>
						<div class="form-group col-md-4">
							<label for="edu_level">學制*</label>
							@switch( auth()->user()->edu_level )
								@case('B')
								<input id="edu_level" type="text" readonly class="form-control-plaintext" name="edu_level" value="大學部">
								@break
								@case('M')
								<input id="edu_level" type="text" readonly class="form-control-plaintext" name="edu_level" value="碩士班">
								@break
								@case('D')
								<input id="edu_level" type="text" readonly class="form-control-plaintext" name="edu_level" value="博士班">
								@break
								@default
								-
							@endswitch
						</div>
					</div>

					<div class="custom-control custom-checkbox mb-3">
						<input type="checkbox" class="custom-control-input" id="check" name="check" required v-model="check">
						<label class="custom-control-label" for="check">我已確認資料無誤</label>
						@if ($errors->has('last_name_en'))
							<div class="invalid-feedback"> {{ $errors->first('last_name_en') }} </div>
						@endif
					</div>

					<div class="form-group row" v-show="check">
						<div class="col-sm-10">
							<button type="submit" class="btn btn-primary">報名考試</button>
						</div>
					</div>
				</form>
				@endrole
			@else
				<div class="alert alert-info">
					<i class="fas fa-info-circle fa-fw"></i> 請先完成信箱認證才能報名
				</div>
			@endif
		@else
			<div class="alert alert-info">
				<i class="fas fa-info-circle fa-fw"></i> 請先 <a href="#" data-toggle="modal" data-target="#LoginModal">登入</a> 才能報名
			</div>
		@endauth
	@else
		<div class="alert alert-danger">
			<i class="fas fa-info-circle fa-fw"></i> 報名已截止
		</div>
	@endif
@endsection

@push('script')
	@auth
		@if( Auth::user()->verified )
			<script>
				const app = new Vue({
					el: '#app',
					data: {
						check: 0,
					},
				});
			</script>
		@endif
	@endauth
@endpush
