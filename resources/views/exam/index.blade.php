@extends('layouts.app')

@section('page-title', '所有考試')

@section('page-breadcrumb')
	<ol class="breadcrumb hide-phone p-0 m-0">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">首頁</a></li>
		<li class="breadcrumb-item active">所有考試</li>
	</ol>
@endsection

@section('content')
	<table id="exam" class="table table-striped table-bordered">
		<thead>
		<tr>
			<th>#</th>
			<th>名稱</th>
			<th>考試時間</th>
			<th>報名時間</th>
		</tr>
		</thead>
	</table>
@endsection

@push('script')
	<script>
		$(function () {
			$('#exam').DataTable({
				processing: true,
				serverSide: true,
				ajax: '{{ route('api.exam') }}',
				columns: [
					{data: 'index'},
					{data: 'name'},
					{data: 'time'},
					{data: 'signup_time'}
				],
				language: {
					"url": "{{ route('js.datatable') }}"
				}
			});
		});
	</script>
@endpush