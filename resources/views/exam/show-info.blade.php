<h2 class="display-4">{{ $exam->name }}</h2>
<hr class="my-4">
<div>
	<i class="fas fa-map-marker-alt fa-fw"></i> 考試地點：{{ $exam->time_end }}
</div>
<div class="mb-3">
	<i class="fas fa-calendar-alt fa-fw"></i> 考試時間：{{ $exam->time_start->format('Y-m-d H:i') }} ~ {{ $exam->time_end->format('Y-m-d H:i') }}
</div>
<div class="mt-3 mb-3">
	<i class="fas fa-calendar-check"></i> 報名時間：{{ $exam->signup_start->format('Y-m-d H:i') }} ~ {{ $exam->signup_end->format('Y-m-d H:i') }}
</div>
<div class="card">
	<div class="card-body">
		{!! $exam->desc !!}
	</div>
</div>
