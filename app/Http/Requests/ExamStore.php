<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExamStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'           => 'required',
            'place'          => 'required',
            'time_start'     => 'required|date_format:"Y-m-d H:i"|before:time_end',
            'time_end'       => 'required|date_format:"Y-m-d H:i"',
            'signup_start'   => 'required|date_format:"Y-m-d H:i"|before:signup_end',
            'signup_end'     => 'required|date_format:"Y-m-d H:i"',
//            'pass_threshold' => 'required|min:0|integer',
            'publish_time'   => 'required|date_format:"Y-m-d H:i"|after:time_end',
            'exam_lang'   => 'required|array',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'           => '名稱',
            'place'          => '地點',
            'time_start'     => '考試時間：開始時間',
            'time_end'       => '考試時間：結束時間',
            'signup_start'   => '報名時間：開始時間',
            'signup_end'     => '報名時間：截止時間',
            'pass_threshold' => '通過門檻',
            'publish_time'   => '成績公布時間',
        ];
    }
}
