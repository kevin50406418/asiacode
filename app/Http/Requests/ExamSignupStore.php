<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ExamSignupStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if( !auth()->check() ){
            return redirect()->route('login')->with('error_msg','請先登入');
        }

        if( !auth()->user()->verified ){
            return redirect()->back()->with('error_msg','請先完成信箱認證才能報名');
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $exam_id = $this->route('id');

        return [
            'coding_lang' => [
                'required',
                'string',
                Rule::exists('exam_coding_lang', 'lang_id')->where(function ($query) use ($exam_id){
                    $query->where('exam_id', $exam_id);
                }),
            ],
            'check'       => 'accepted'
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'coding_lang' => '報考程式語言',
            'check'       => '我已確認資料無誤'
        ];
    }
}
