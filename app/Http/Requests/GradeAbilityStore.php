<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GradeAbilityStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug'             => 'required|alpha|max:5|min:1',
            'cert_title_en'    => 'required',
            'cert_title_zh_tw' => 'required',
            'desc'             => 'required',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'slug'             => '證照階級代碼',
            'cert_title_en'    => '證照英文標題',
            'cert_title_zh_tw' => '證照中文標題',
            'desc'             => '能力指標說明',
        ];
    }
}
