<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserProfileUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $school_id = $this->input('school_id');

        return [
            //            'username'      => 'required|string|max:255|unique:users',
            'name_zh_tw'    => 'required|string|max:255',
            'last_name_en'  => 'required|string|max:255',
            'first_name_en' => 'required|string|max:255',
            'school_id'     => [
                'required',
                'string',
                Rule::exists('school', 'id')->where(function ($query){
                    $query->where('open', 1);
                    //                        ->whereNotNull('student_email');
                }),
            ],
            'dept_id'       => 'required|string|exists:school_dept,id',
            'edu_level'     => 'required|string|in:B,M,D',
            'student_id'    => [
                'required',
                'max:255',
                'string',
                Rule::unique('users')->where(function ($query) use ($school_id){
                    return $query->whereNotIn('school_id', [$school_id]);
                })
            ],
            'sex'           => 'required|string|in:0,1',
            'email'         => 'required|email|max:255|unique:users,email,' . $this->route('user') . ',id',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'username'      => '帳號',
            'name_zh_tw'    => '姓名',
            'last_name_en'  => '英文名',
            'first_name_en' => '英文姓',
            'email'         => '信箱',
            'school_id'     => '學校',
            'dept_id'       => '學系',
            'edu_level'     => '學制',
            'student_id'    => '學號',
            'sex'           => '性別',
            'password'      => '密碼',
        ];
    }
}
