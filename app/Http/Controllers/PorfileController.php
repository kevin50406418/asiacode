<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfilePasswdUpdate;
use App\School;
use App\SchoolDept;
use App\User;
use Hash;
use Illuminate\Http\Request;

class PorfileController extends Controller
{
    public function index()
    {
        $schools = School::where('open', 1)->whereNotNull('student_email')->get([
            'id',
            'name',
            'student_email'
        ])->keyBy('id');
        $depts   = SchoolDept::where(['id' => auth()->user()->dept_id])->get()->keyBy('id');
        $user    = auth()->user();

        return view('profile.index', compact('user', 'schools', 'depts'));
    }

    public function passwd(ProfilePasswdUpdate $request){
        if ( Hash::check($request->input('old_password'), auth()->user()->getAuthPassword()) ) {

            $request->user()->fill([
                'password' => Hash::make($request->input('password'))
            ])->save();

            return redirect()->route('profile.index')->with('msg','密碼更改成功, 請使用新密碼登入');
        } else {
            // old password incorrect
            return redirect()->back()->with('error_msg', '密碼更改失敗');
        }
    }
}
