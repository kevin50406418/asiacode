<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DatatableController extends Controller
{
    public function __invoke()
    {
        return response()->view('datatable')->header('Content-Type', 'application/json; charset=utf-8');
    }
}
