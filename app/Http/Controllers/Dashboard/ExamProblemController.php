<?php

namespace App\Http\Controllers\Dashboard;

use App\Exam;
use App\ExamProblem;
use App\Problem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExamProblemController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exam = Exam::findOrFail($id);
        $problems = Problem::all();
        $exam_problems = ExamProblem::with('problem')->whereExamId($id)->get();

        $problem_id =  $exam_problems->pluck('problem_id');
        $show = [];
        foreach ($problem_id as $pid){
            $show[$pid] = true;
        }

        return view('dashboard.exam-problem.show', compact('exam','exam_problems', 'problems','show'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $exam = Exam::findOrFail($id);

        foreach ($request->input('problem_id') as $problem_id){
            ExamProblem::create([
                'exam_id' => $exam->id,
                'problem_id' => $problem_id,
            ]);
        }

        return redirect()->route('dashboard.exam-problem.show', $exam->id)->with('msg', '考試題目設定完畢');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $msg = ['success' => 0, 'message' => '刪除考試題目失敗', 'status' => 'error'];
        $exam = Exam::whereId($id)->first();

        if (empty($exam)) {
            return response()->json($msg);
        }

        if( ExamProblem::whereExamId($exam->id)->whereProblemId($request->query('problem_id'))->delete() ){
            $msg = ['success' => 1, 'message' => '成功刪除考試題目', 'status' => 'success'];
        }else{
            $msg = ['success' => 0, 'message' => '刪除考試題目失敗', 'status' => 'error'];
        }

        return response()->json($msg);
    }
}
