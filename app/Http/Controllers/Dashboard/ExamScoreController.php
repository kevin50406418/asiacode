<?php

namespace App\Http\Controllers\Dashboard;

use App\Exam;
use App\ExamCert;
use App\ExamSignup;
use App\ExamCodingLang;
use App\GradeAbility;
use App\Http\Requests\ExamScoreStore;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;

class ExamScoreController extends Controller
{
    public function index(Request $request, $id)
    {
        $exam = Exam::findOrFail($id);

        $signups = ExamSignup::whereExamId($exam->id)->get();
        $langs   = ExamCodingLang::with('lang')->where('exam_id', $id)->get()->keyBy('lang_id');

        $lists = collect();
        //        $string = '2002,1,3,5.0,3.0
        //2001,2,2,0.0,0.0
        //2003,3,0,0.0,0.0
        //2004,3,0,0.0,0.0
        //2005,3,0,0.0,0.0
        //2006,3,0,0.0,0.0
        //2007,3,0,0.0,0.0
        //2008,3,0,0.0,0.0
        //2009,3,0,0.0,0.0
        //2010,3,0,0.0,0.0
        //';

        if ($request->hasFile('score')) {
            $file = $request->file('score');

            $string = file_get_contents($file->getRealPath());
            $string = explode("\n", $string);
            array_pop($string);
            collect($string)->each(function ($item) use ($lists, $exam){
                $item    = explode(',', $item);
                $account = data_get($item, '0', null);
                if ( !is_null($account) && is_numeric($account)) {
                    $account = $account - $exam->id * 1000;
                    $lists->put($account, data_get($item, '2', 0));
                }
            });
        }
//        else{
//            return redirect()->route('dashboard.exam.show', $exam->id)->with('error_msg', '請上傳檔案');
//        }

        return view('dashboard.exam-score.index', compact('exam', 'signups', 'langs', 'lists'));
    }

    public function store(ExamScoreStore $request, $id)
    {
        $exam    = Exam::findOrFail($id);
        $signups = ExamSignup::whereExamId($exam->id)->get();

        foreach ($signups as $key => $signup) {
            $signup->solve_problem = $request->input('solve_problem.' . $signup->pc2_account, 0);
            $signup->save();
        }

        return redirect()->route('dashboard.exam.show', $exam->id)->with('msg', '輸入成績完畢');
    }

    public function cert($id)
    {
        $exam    = Exam::findOrFail($id);
        $signups = ExamSignup::with('code')->whereExamId($exam->id)->get();

        if (is_null($exam->pass_threshold)) {
            return redirect()->route('dashboard.exam.show', $exam->id)->with('error_msg', '請先設定通過門檻');
        }

        $grade = GradeAbility::whereExamId($exam->id)->first();
        if (is_null($grade)) {
            return redirect()->route('dashboard.exam.show', $exam->id)->with('error_msg', '請先完成成績說明設定');
        }

        $year     = Carbon::parse($exam->time_start)->year;
        $grade_id = $grade->slug;

        $cert = ExamCert::all()->pluck('number', 'cert_id');

        foreach ($signups as $key => $signup) {
            // 當是空值 才配發證書編號 and 達到通過門檻
            if ($signup->solve_problem >= $exam->pass_threshold && is_null($signup->cert_id)) {
                $lang                              = $signup->code->slug;
                $cert_id                           = array_get($cert, $grade_id . $lang . $year, 0);
                $cert[ $grade_id . $lang . $year ] = $cert_id + 1;

                $signup->cert_id = $cert[ $grade_id . $lang . $year ];
                $signup->cert_key_1 = str_random(8);
                $signup->cert_key_2 = str_random(10);
                $signup->save();
            }
        }

        // 回存配發字號
        foreach ($cert as $key => $number) {
            ExamCert::updateOrCreate(
                ['cert_id' => $key],
                ['number' => $number]
            );
        }

        return redirect()->route('dashboard.exam.show', $exam->id)->with('msg', '證書產生完畢');
    }

    public function cert_file($id)
    {
        $exam    = Exam::findOrFail($id);

        if (is_null($exam->pass_threshold)) {
            return redirect()->route('dashboard.exam.show', $exam->id)->with('error_msg', '請先設定通過門檻');
        }

        $grade = GradeAbility::whereExamId($exam->id)->first();
        if (is_null($grade)) {
            return redirect()->route('dashboard.exam.show', $exam->id)->with('error_msg', '請先完成成績說明設定');
        }

        $signups = ExamSignup::with('code')->whereExamId($exam->id)->whereNotNull('cert_id')->get();
        $langs = ExamCodingLang::with('lang')->where('exam_id', $id)->get()->keyBy('lang_id');

        $certificate = view('certificate.all', compact('exam', 'signups', 'langs', 'grade'));

//        return $certificate;
        return PDF::loadHTML($certificate)
            ->setPaper('a4')
            ->setOrientation('landscape')
            ->setOption('disable-javascript', true)
            ->inline('certificate.pdf');
    }
}
