<?php

namespace App\Http\Controllers\Dashboard;

use App\Problem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GitHub;

class ProblemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.problem.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $problem = Problem::with('samples')->whereProblemId($id)->first();

        if (empty($problem)) {
            return redirect()->route('dashboard.problem.index')->with('error_msg', '找不到題庫');
        }

        try {
            $testcases = GitHub::api('repo')->contents()->show(env('GITHUB_REPO_OWNER'), env('GITHUB_REPO_NAME'),
                'OneToHundred/' . str_pad($id, 3, "0", STR_PAD_LEFT), env('GITHUB_BRANCHE'));
        } catch (\Github\Exception\RuntimeException $e) {
            $testcases = [];
        }

        $testcases = collect($testcases)->mapWithKeys(function ($item){
            return [$item[ 'name' ] => $item[ 'download_url' ]];
        });
        $testcases = $testcases->all();

        return view('dashboard.problem.show', compact('problem', 'testcases'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download($id)
    {
        $problem = Problem::whereProblemId($id)->first();

        if (empty($problem)) {
            return redirect()->back()->with('error_msg', '找不到題庫');
        }

        try {
            $testcases = GitHub::api('repo')->contents()->show(env('GITHUB_REPO_OWNER'), env('GITHUB_REPO_NAME'),
                'OneToHundred/' . str_pad($id, 3, "0", STR_PAD_LEFT), env('GITHUB_BRANCHE'));
        } catch (\Github\Exception\RuntimeException $e) {
            return view('dashboard.problem.notfound');
        }

        dd($testcases);

        //        return response(GitHub::api('repo')->contents()->download('ASIAH304', 'OnlineJudge_testcase',
        //            'OneToHundred/' . $id, '105'))
        //            ->header('Content-type', 'application/zip')
        //            ->header('Content-Disposition', 'attachment; filename="' . $id . '.zip"');
    }
}
