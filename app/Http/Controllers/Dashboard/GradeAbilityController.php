<?php

namespace App\Http\Controllers\Dashboard;

use App\Exam;
use App\GradeAbility;
use App\Http\Requests\GradeAbilityStore;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GradeAbilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exam = Exam::find($id);

        if (empty($exam)) {
            return redirect()->back()->with('error_msg', '考試資訊不存在');
        }

        $grade = GradeAbility::whereExamId($exam->id)->first();

        return view('dashboard.grade-ability.edit', compact('exam', 'grade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(GradeAbilityStore $request, $id)
    {
        $exam = Exam::find($id);

        if (empty($exam)) {
            return redirect()->back()->with('error_msg', '考試資訊不存在');
        }
        $ability = [
            'slug'             => $request->slug,
            'cert_title_en'    => $request->cert_title_en,
            'cert_title_zh_tw' => $request->cert_title_zh_tw,
            'desc'             => $request->desc,
        ];

        if( GradeAbility::updateOrCreate( $ability, ['exam_id' => $exam->id]) ){
            return redirect()->route('dashboard.grade-ability.edit', $exam->id)->with('msg', '成績說明設定成功');
        }else{
            return redirect()->back()->with('error_msg', '成績說明設定失敗');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
