<?php

namespace App\Http\Controllers\Dashboard;

use App\CodingLang;
use App\Exam;
use App\ExamCodingLang;
use App\ExamSignup;
use App\Http\Requests\ExamStore;
use App\Http\Controllers\Controller;
use App\Http\Requests\ExamThresholdUpdate;
use Carbon\Carbon;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exams = Exam::all();

        return view('dashboard.exam.index', compact('exams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $langs = CodingLang::all();

        return view('dashboard.exam.create', compact('langs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ExamStore $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ExamStore $request)
    {
        $exam                 = new Exam();
        $exam->name           = $request->input('name');
        $exam->place          = $request->input('place');
        $exam->desc           = $request->input('desc') ?? '';
        $exam->time_start     = Carbon::parse($request->input('time_start'));
        $exam->time_end       = Carbon::parse($request->input('time_end'));
        $exam->signup_start   = Carbon::parse($request->input('signup_start'));
        $exam->signup_end     = Carbon::parse($request->input('signup_end'));
        $exam->pass_threshold = null;
        $exam->publish_time   = Carbon::parse($request->input('publish_time'));
        $exam->problem_note   = $request->input('problem_note') ?? '';
        $exam->save();

        $coding = [];

        foreach ($request->input('exam_lang') as $key => $exam_lang) {
            $coding[ $key ] = [
                'exam_id' => $exam->id,
                'lang_id' => $exam_lang,
            ];
        }

        return ExamCodingLang::insert($coding) ? redirect()->route('dashboard.exam.index')->with('msg',
            '建立考試成功') : redirect()->route('dashboard.exam.index')->with('error_msg', '建立考試失敗');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exam = Exam::findOrFail($id);

        $langs = ExamCodingLang::with('lang')->where('exam_id', $id)->get()->keyBy('lang_id');

        $signups = ExamSignup::whereExamId($exam->id)->get();
        $pc2     = ExamSignup::whereExamId($exam->id)->whereNotNull('pc2_account')->count();

        $data = $signups->groupBy(function ($item, $key) {
            return $item['solve_problem'];
        })->map(function ($item, $key) {
            return collect($item)->count();
        });

        return view('dashboard.exam.show', compact('exam', 'langs', 'signups', 'pc2', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exam = Exam::findOrFail($id);

        $langs = CodingLang::all();

        $coding = ExamCodingLang::whereExamId($id)->get()->pluck('lang_id');

        return view('dashboard.exam.edit', compact('exam', 'langs', 'coding'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(ExamStore $request, $id)
    {
        $exam                 = Exam::findOrFail($id);
        $exam->name           = $request->input('name');
        $exam->place          = $request->input('place');
        $exam->desc           = $request->input('desc') ?? '';
        $exam->time_start     = Carbon::parse($request->input('time_start'));
        $exam->time_end       = Carbon::parse($request->input('time_end'));
        $exam->signup_start   = Carbon::parse($request->input('signup_start'));
        $exam->signup_end     = Carbon::parse($request->input('signup_end'));
        $exam->pass_threshold = $request->input('pass_threshold');
        $exam->publish_time   = Carbon::parse($request->input('publish_time'));
        $exam->problem_note   = $request->input('problem_note') ?? '';
        $exam->save();

        ExamCodingLang::whereExamId($id)->delete();

        $coding = [];
        foreach ($request->input('exam_lang') as $key => $exam_lang) {
            $coding[ $key ] = [
                'exam_id' => $exam->id,
                'lang_id' => $exam_lang,
            ];
        }

        return ExamCodingLang::insert($coding) ? redirect()->route('dashboard.exam.edit', $id)->with('msg',
            '更新考試成功') : redirect()->route('dashboard.exam.edit', $id)->with('error_msg', '更新考試失敗');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function update_threshold(ExamThresholdUpdate $request, $id)
    {
        $exam                 = Exam::findOrFail($id);
        $exam->pass_threshold = $request->input('pass_threshold');

        $msg = ['success' => 0, 'message' => '更新通過門檻失敗', 'status' => 'error'];
        if ($exam->save()) {
            $msg = ['success' => 1, 'message' => '更新通過門檻成功', 'status' => 'success'];
        }

        return response()->json($msg);;
    }
}
