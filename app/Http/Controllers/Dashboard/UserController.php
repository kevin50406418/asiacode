<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\UserProfileUpdate;
use App\School;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('dashboard.user.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $schools = School::where('open', 1)->whereNotNull('student_email')->get([
            'id',
            'name',
            'student_email'
        ])->keyBy('id');

        return view('dashboard.user.edit', compact('schools', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserProfileUpdate $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UserProfileUpdate $request, $id)
    {
        $user = User::findOrFail($id);

        $user->name_zh_tw    = $request->input('name_zh_tw');
        $user->last_name_en  = $request->input('last_name_en');
        $user->first_name_en = $request->input('first_name_en');
        $user->email         = $request->input('email');
        $user->school_id     = $request->input('school_id');
        $user->dept_id       = $request->input('dept_id');
        $user->edu_level     = $request->input('edu_level');
        $user->student_id    = $request->input('student_id');
        $user->sex           = $request->input('sex');
        $user->edu_level     = $request->input('edu_level');
        $user->verified     = $request->input('verified', 0) ? 1 : 0;

        return $user->save() ? redirect()->route('dashboard.user.edit', $id)->with('msg',
            '成功修改使用者資料') : redirect()->back()->with('error_msg', '修改使用者資料失敗');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
