<?php

namespace App\Http\Controllers\Dashboard;

use App\Exam;
use App\ExamSignup;
use App\Http\Controllers\Controller;
use Excel;

class ExamExportController extends Controller
{
    public function index($id)
    {
        $exam    = Exam::findOrFail($id);
        $signups = ExamSignup::with(['school', 'dept', 'code'])->whereExamId($exam->id)->get();

        $lists = collect();
        $lists->push(['學校', '學系', '學制', '學號', '姓名', '程式語言', '報名時間']);

        foreach ($signups as $signup) {
            switch ($signup->edu_level) {
                case 'B':
                    $edu_level = '大學部';
                    break;
                case 'M':
                    $edu_level = '碩士班';
                    break;
                default:
                case 'D':
                    $edu_level = '博士班';
                    break;
            }
            $lists->push([
                $signup->school->name,
                $signup->dept->name,
                $edu_level,
                $signup->student_id,
                $signup->name_zh_tw,
                $signup->code->lang,
                $signup->created_at->toDateTimeString()
            ]);
        }
        $lists = $lists->toArray();

        Excel::create($exam->name . ' - 報名名單(' . now()->toDateTimeString() . ')', function ($excel) use ($lists, $exam){
            $excel->setTitle($exam->name . ' - 報名名單')
                ->setCreator(config('asiacode.site_name'))
                ->setCompany(config('asiacode.site_name'))
                ->setManager(config('asiacode.site_name'))
                ->setLastModifiedBy(config('asiacode.site_name'));

            $excel->sheet('報名名單', function ($sheet) use ($lists){
                $sheet->rows($lists)->setAutoSize(true);
            });
        })->export('xlsx');
    }


    public function pc2($id)
    {
        $exam = Exam::findOrFail($id);

        $this->generate_pc2($id);

        $signups = ExamSignup::with(['school', 'dept', 'code'])->whereExamId($exam->id)->get();


        $lists = collect();
        $lists->push([
            'site',
            'account',
            'displayname',
            'password',
            'group',
            'permdisplay',
            'permlogin',
            'externalid',
            'alias',
            'permpassword',
            'longschoolname',
            'shortschoolname',
            'countrycode'
        ]);

        foreach ($signups as $signup) {
            $lists->push([
                $exam->id,
                'team' . $signup->pc2_account,
                $signup->student_id,
                $signup->pc2_password,
                '',
                'TRUE',
                'TRUE',
                $exam->id*1000+$signup->pc2_account,
                $signup->student_id,
                'FALSE',
                '',
                '',
                'XXX'
            ]);
        }

        $lists = $lists->toArray();

        return response()
            ->view('dashboard.exam-export.pc2', compact('lists'), 200)
            ->header('Content-Disposition', 'attachment; filename="site'.$exam->id . '.txt"')
            ->header('Content-Type', 'text/plain');
    }

    protected function generate_pc2($id)
    {
        $exam = Exam::findOrFail($id);

        $signups = ExamSignup::whereExamId($exam->id)->get();

        $size    = count($signups);
        $numbers = range(1, $size);
        shuffle($numbers);

        $characters       = '23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'; //去除 1, i, l, I, o, 0, O
        $characters_array = str_split($characters);


        foreach ($signups as $key => $signup) {
            $signup->pc2_account  = $numbers[ $key ];
            $signup->pc2_password = join('', array_random($characters_array, env('PC2_PASSWORD_LENGTH')));
            $signup->save();
        }

    }
}
