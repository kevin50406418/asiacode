<?php

namespace App\Http\Controllers\Api;

use App\Exam;
use App\ExamCodingLang;
use App\ExamSignup;
use App\Http\Controllers\Controller;
use DataTables;

class ExamController extends Controller
{
    public function all()
    {
        $exams      = Exam::all();
        $datatables = collect();

        foreach ($exams as $key => $exam) {
            $datatables->push([
                'index'       => $key + 1,
                'name'        => '<a href="' . route('exam.show', $exam->id) . '">' . $exam->name . '</a>',
                'time'        => $exam->time_start->format('Y-m-d H:i') . ' ~ ' . $exam->time_end->format('Y-m-d H:i'),
                'signup_time' => $exam->signup_start->format('Y-m-d H:i') . ' ~ ' . $exam->signup_end->format('Y-m-d H:i'),
            ]);
        }

        return DataTables::of($datatables)->rawColumns(['name'])->make(true);
    }

    public function admin()
    {
        $exams      = Exam::all();
        $datatables = collect();

        foreach ($exams as $key => $exam) {
            $datatables->push([
                'index'       => $exam->id,
                'name'        => $exam->name,
                'time'        => $exam->time_start->format('Y-m-d H:i') . ' ~ ' . $exam->time_end->format('Y-m-d H:i'),
                'signup_time' => $exam->signup_start->format('Y-m-d H:i') . ' ~ ' . $exam->signup_end->format('Y-m-d H:i'),
            ]);
        }

        return DataTables::of($datatables)->addColumn('action', function ($datatable){
            $return = '<div class="btn-group" role="group">';
            $return.= '<a class="btn btn-success" href="' . route('dashboard.exam.edit', $datatable[ 'index' ]) . '">修改</a>';
            $return.= '<a class="btn btn-secondary" href="' . route('dashboard.exam.show', $datatable[ 'index' ]) . '">名單</a>';
            $return.= '<a class="btn btn-info" href="' . route('dashboard.exam-problem.show', $datatable[ 'index' ]) . '">題目設定</a>';
            $return.= '</div>';
            return $return;
        })->rawColumns(['action'])->make(true);
    }

    public function signup($id)
    {
        $signups    = ExamSignup::whereExamId($id)->get();
        $datatables = collect();

        $langs = ExamCodingLang::with('lang')->where('exam_id', $id)->get()->keyBy('lang_id');
        foreach ($signups as $key => $signup) {
            $datatables->push([
                'index'         => $key + 1,
                'student_id'    => $signup->student_id,
                'name_zh_tw'    => $signup->name_zh_tw,
                'name_en'       => $signup->first_name_en . ' ' . $signup->last_name_en,
                'lang'          => $langs[ $signup->lang_id ]->lang->lang,
                'solve_problem' => $signup->solve_problem,
                'cert_id'       => $signup->cert_id,
                'created_at'    => optional($signup->created_at)->toDateTimeString(),
                'deleted_at'    => optional($signup->deleted_at)->toDateTimeString(),
                'has_pc_2'      => !is_null($signup->pc2_account),
            ]);
        }

        return DataTables::of($datatables)->setRowClass(function ($signup){
            return $signup[ 'has_pc_2' ] ? '' : 'bg-danger';
        })->make(true);
    }
}
