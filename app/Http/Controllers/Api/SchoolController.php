<?php

namespace App\Http\Controllers\Api;

use App\School;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;

class SchoolController extends Controller
{
    public function all()
    {
        $schools    = School::orderBy('open','desc')->get();
        $datatables = collect();


        foreach ($schools as $key => $school) {
            $datatables->push([
                'id'   => $school->id,
                'name' => $school->name,
                'open' => $school->open ? 'Y' : 'N',
            ]);
        }

        return DataTables::of($datatables)
            ->addColumn('action', function ($datatable){
                $return = '<a class="btn btn-sm btn-info" href="' . route('dashboard.school.edit',
                        $datatable[ 'id' ]) . '">編輯</a>';
                $return.= '<a class="btn btn-sm btn-info" href="' . route('dashboard.school.show',
                        $datatable[ 'id' ]) . '">學系管理</a>';
                return $return;
            })
            ->make(true);
    }
}
