<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use DataTables;
use App\Problem;

class ProblemController extends Controller
{
    public function all()
    {
        $problems = Problem::whereDefunct('N')->get();
        $datatables = collect();

        foreach ($problems as $key => $problem) {
            $datatables->push([
                'problem_id' => $problem->problem_id,
                'title'      => '<a href="' . route('problem.show', $problem->problem_id) . '">' . $problem->title . '</a>',
            ]);
        }

        return DataTables::of($datatables)->rawColumns(['title'])->make(true);
    }

    public function dashboard()
    {
        $problems = Problem::all();
        $datatables = collect();

        foreach ($problems as $key => $problem) {
            $datatables->push([
                'problem_id' => $problem->problem_id,
                'title'      => '<a href="' . route('dashboard.problem.show', $problem->problem_id) . '">' . $problem->title . '</a>',
            ]);
        }

        return DataTables::of($datatables)->rawColumns(['title'])->make(true);
    }
}
