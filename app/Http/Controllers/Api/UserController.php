<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\User;

class UserController extends Controller
{
    public function all()
    {
        $users      = User::all();
        $datatables = collect();


        foreach ($users as $key => $user) {
            $datatables->push([
                'id' => $user->id,
                'student_id' => $user->student_id,
                'username'   => $user->username,
                'name_zh_tw' => $user->name_zh_tw,
                'verified'   => $user->verified ? 'Y' : 'N',
            ]);
        }

        return DataTables::of($datatables)
            ->addColumn('action', function ($datatable){
                $return= '<a class="btn btn-sm btn-info" href="' . route('dashboard.user.edit', $datatable[ 'id' ]) . '">編輯</a>';
                return $return;
            })
            ->make(true);
    }
}
