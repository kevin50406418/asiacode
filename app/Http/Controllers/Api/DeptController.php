<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\School;

class DeptController extends Controller
{
    public function get(Request $request){
        $school_id = $request->input('school_id', '');

        $school = School::with('depts')->where(['id' => $school_id, 'open' => 1])->whereNotNull('student_email')->first();

        if( optional($school)->depts ){
            return response()->json($school->depts);
        }

        return response()->json([]);
    }
}
