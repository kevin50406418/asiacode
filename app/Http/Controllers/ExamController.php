<?php

namespace App\Http\Controllers;

use App\Exam;
use App\ExamCodingLang;
use App\GradeAbility;
use App\Http\Requests\ExamSignupStore;
use App\School;
use App\SchoolDept;
use App\ExamSignup;
use Carbon\Carbon;
use PDF;

class ExamController extends Controller
{
    public function index()
    {
        $exams = Exam::all();

        return view('exam.index', compact('exams'));
    }

    public function show($id)
    {
        $exam = Exam::find($id);

        if (empty($exam)) {
            return redirect()->back()->with('error_msg', '考試資訊不存在');
        }

        if (auth()->check()) {
            $schools = School::where('open', 1)->whereNotNull('student_email')->get([
                'id',
                'name',
                'student_email'
            ])->keyBy('id');
            $langs   = ExamCodingLang::with('lang')->where('exam_id', $id)->get();
            $depts   = SchoolDept::where(['id' => auth()->user()->dept_id])->get()->keyBy('id');

            $signup = ExamSignup::whereUserId(auth()->id())->whereExamId($exam->id)->first();

            if ($signup) {
                return view('exam.finish', compact('exam', 'schools', 'depts', 'langs', 'signup'));
            }
        }

        return view('exam.show', compact('exam', 'schools', 'depts', 'langs', 'signup'));
    }

    public function store(ExamSignupStore $request, $id)
    {
        $exam = Exam::find($id);

        if (empty($exam)) {
            return redirect()->back()->with('error_msg', '考試資訊不存在');
        }

        if ( !$exam->can_signup) {
            return redirect()->back()->with('error_msg', '報名已截止');
        }

        $signup_check = ExamSignup::whereUserId(auth()->id())->whereExamId($exam->id)->first();
        if ($signup_check) {
            return redirect()->back()->with('error_msg', '你已報名完成!!');
        }

        $signup = new ExamSignup();

        $signup->exam_id       = $exam->id;
        $signup->user_id       = auth()->id();
        $signup->lang_id       = $request->input('coding_lang');
        $signup->name_zh_tw    = auth()->user()->name_zh_tw;
        $signup->last_name_en  = auth()->user()->last_name_en;
        $signup->first_name_en = auth()->user()->first_name_en;
        $signup->school_id     = auth()->user()->school_id;
        $signup->dept_id       = auth()->user()->dept_id;
        $signup->school_id     = auth()->user()->school_id;
        $signup->edu_level     = auth()->user()->edu_level;
        $signup->edu_level     = auth()->user()->edu_level;
        $signup->student_id    = auth()->user()->student_id;
        $signup->sex           = auth()->user()->sex;
        $signup->id_number           = auth()->user()->id_number;

        if ($signup->save()) {
            return redirect()->back()->with('msg', '報名考試成功');
        } else {
            return redirect()->back()->with('error_msg', '報名考試失敗');
        }
    }

    public function certificate($id)
    {
        $exam = Exam::find($id);

        if (empty($exam)) {
            return redirect()->back()->with('error_msg', '考試資訊不存在');
        }

        if (auth()->check()) {
            $grade  = GradeAbility::whereExamId($exam->id)->first();
            $signup = ExamSignup::whereUserId(auth()->id())->whereExamId($exam->id)->first();

            if ( !$signup) {
                return redirect()->route('exam.show', $exam->id)->with('error_msg', '您並未報名考試');
            }

            if ( !now()->gt(Carbon::parse($exam->publish_time)) || is_null($signup->solve_problem) || is_null($exam->pass_threshold)) {
                return redirect()->route('exam.show', $exam->id)->with('error_msg', '尚未有通過證書');
            }

            if ($signup->solve_problem < $exam->pass_threshold) {
                return redirect()->route('exam.show', $exam->id)->with('error_msg', '您未通過考試');
            }

            $langs = ExamCodingLang::with('lang')->where('exam_id', $id)->get()->keyBy('lang_id');

            $certificate = view('certificate.pass', compact('exam', 'signup', 'langs', 'grade'));

            return PDF::loadHTML($certificate)
                ->setPaper('a4')
                ->setOrientation('landscape')
                ->setOption('disable-javascript', true)
                ->inline('certificate.pdf');
        } else {
            return redirect()->back()->with('error_msg', '請先登入');
        }
    }

    public function cancel($id)
    {
        $exam = Exam::find($id);

        if (empty($exam)) {
            return redirect()->back()->with('error_msg', '考試資訊不存在');
        }

        $signup = ExamSignup::whereUserId(auth()->id())->whereExamId($exam->id)->first();

        if ($signup) {
            if ($signup->delete()) {
                return redirect()->route('exam.show', $exam->id)->with('msg', '取消報名成功');
            } else {
                return redirect()->route('exam.show', $exam->id)->with('error_msg', '取消報名失敗');
            }
        } else {
            return redirect()->route('exam.show', $exam->id)->with('error_msg', '您並未報名考試');
        }

    }
}
