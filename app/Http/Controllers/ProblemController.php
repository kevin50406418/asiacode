<?php

namespace App\Http\Controllers;

use App\Problem;

class ProblemController extends Controller
{
    public function index(){
        return view('problem.index');
    }

    public function show($id){
        $problem = Problem::with('samples')->whereProblemId($id)->whereDefunct('N')->first();

        if( empty($problem) ){
            return redirect()->route('problem.index')->with('error_msg','找不到題庫');
        }
        return view('problem.show',compact('problem'));
    }
}
