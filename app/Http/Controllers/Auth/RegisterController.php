<?php

namespace App\Http\Controllers\Auth;

use App\Role;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\School;
use Illuminate\Validation\Rule;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;
use Jrean\UserVerification\Exceptions\UserNotFoundException;
use Jrean\UserVerification\Exceptions\UserIsVerifiedException;
use Jrean\UserVerification\Exceptions\TokenMismatchException;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use VerifiesUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username'      => 'required|string|max:255|unique:users',
            'name_zh_tw'    => 'required|string|max:255',
            'last_name_en'  => 'required|string|max:255',
            'first_name_en' => 'required|string|max:255',
            'school_id'     => [
                'required',
                'string',
                Rule::exists('school', 'id')->where(function ($query){
                    $query->where('open', 1)->whereNotNull('student_email');
                }),
            ],
            'dept_id'       => 'required|string|exists:school_dept,id',
            'edu_level'     => 'required|string|in:B,M,D',
            'student_id'    => [
                'required',
                'max:255',
                'string',
                Rule::unique('users')->where(function ($query) use ($data){
                    return $query->whereNotIn('school_id', [$data[ 'school_id' ]]);
                })
            ],
            'sex'           => 'required|string|in:0,1',
            'id_number'     => 'required|string',
            'email'         => 'required|string|email|max:255|unique:users',
            'password'      => 'required|string|min:6|confirmed',
        ], [], [
            'username'      => '帳號',
            'name_zh_tw'    => '姓名',
            'last_name_en'  => '英文名',
            'first_name_en' => '英文姓',
            'email'         => '信箱',
            'school_id'     => '學校',
            'dept_id'       => '學系',
            'edu_level'     => '學制',
            'student_id'    => '學號',
            'sex'           => '性別',
            'id_number'     => '身分證',
            'password'      => '密碼',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return \App\User
     */
    protected function create(array $data)
    {
        //        $school = School::where(['open' => 1, 'id' => $data[ 'school_id' ]])->whereNotNull('student_email')->first();


        $user = User::create([
            'username'      => $data[ 'username' ],
            'name_zh_tw'    => $data[ 'name_zh_tw' ],
            'last_name_en'  => $data[ 'last_name_en' ],
            'first_name_en' => $data[ 'first_name_en' ],
            //            'email'         => $data[ 'student_id' ].'@'.$school->student_email,
            'email'         => $data[ 'email' ],
            'school_id'     => $data[ 'school_id' ],
            'dept_id'       => $data[ 'dept_id' ],
            'edu_level'     => $data[ 'edu_level' ],
            'student_id'    => $data[ 'student_id' ],
            'sex'           => $data[ 'sex' ],
            'id_number'     => $data[ 'id_number' ],
            'password'      => bcrypt($data[ 'password' ]),
        ]);

        $student = Role::whereName('student')->first();
        $user->attachRole($student);

        return $user;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $schools = School::where('open', 1)
            //            ->whereNotNull('student_email')
            ->get([
                'id',
                'name',
                //            'student_email'
            ])->keyBy('id');

        return view('auth.register', compact('schools'));
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Jrean\UserVerification\Exceptions\ModelNotCompliantException
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = $this->create($request->all());

        event(new Registered($user));

        //        $this->guard()->login($user);

        UserVerification::generate($user);

        UserVerification::send($user, '信箱認證');

        return $this->registered($request, $user) ? : redirect($this->redirectPath())->with('msg',
            '帳號成功註冊, 認證信已寄到' . $user->email . '<br>如信箱無認證信, 請確認垃圾郵件');
    }

    public function getVerification(Request $request, $token)
    {
        if ( !$this->validateRequest($request)) {
            return redirect($this->redirectIfVerificationFails());
        }

        try {
            $user = UserVerification::process($request->input('email'), $token, $this->userTable());
        } catch (UserNotFoundException $e) {
            return redirect($this->redirectIfVerificationFails())->with('error_msg', '找不到使用者資料');
        } catch (UserIsVerifiedException $e) {
            return redirect($this->redirectIfVerified())->with('error_msg', '信箱認證失敗');
        } catch (TokenMismatchException $e) {
            return redirect($this->redirectIfVerificationFails())->with('error_msg', 'Token 不正確');
        }

        if (config('user-verification.auto-login') === true) {
            auth()->loginUsingId($user->id);
        }

        return redirect($this->redirectAfterVerification())->with('msg', '信箱完成認證');
    }
}
