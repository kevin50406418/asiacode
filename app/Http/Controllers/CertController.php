<?php

namespace App\Http\Controllers;

use App\ExamSignup;
use App\Http\Requests\CertVerify;

class CertController extends Controller
{
    public function show($key)
    {
        $signup = ExamSignup::where('cert_key_1', $key)->first();

        if (empty($signup)) {
            return redirect()->route('exam.index')->with('error_msg', '查無證書或輸入錯誤的金鑰');
        }

        return view('cert.show', compact('key'));
    }

    public function verify(CertVerify $request, $key)
    {
        $signup = ExamSignup::where('cert_key_1', $key)->first();

        if (empty($signup)) {
            return redirect()->route('exam.index')->with('error_msg', '查無證書或輸入錯誤的金鑰');
        }

        if ($signup->id_number == $request->input('id_number')) {
            return view('cert.verify-valid',compact('signup'));
        } else {
            return redirect()->back()->with('error_msg', '您輸入的資料錯誤');
        }
    }
}
