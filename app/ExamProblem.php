<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ExamProblem
 *
 * @property int $exam_id 考試編號
 * @property int $problem_id 題庫編號
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamProblem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamProblem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamProblem whereExamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamProblem whereProblemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamProblem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ExamProblem extends Model
{
    protected $table = 'exam_problem';
    protected $fillable = [
        'exam_id',
        'problem_id',
        'problem_sort',
    ];

    public function problem()
    {
        return $this->hasOne('App\Problem', 'problem_id', 'problem_id');
    }
}
