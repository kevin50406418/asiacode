<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ExamCert
 *
 * @property string $cert_id AAD ID ex:FJ2018
 * @property int $number 最後配發數字
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamCert whereCertId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamCert whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamCert whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamCert whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ExamCert extends Model
{
    protected $table = 'exam_cert';
    protected $primaryKey = 'cert_id';
    public $incrementing = false;
    protected $fillable = [
        'cert_id',
        'number',
    ];
}
