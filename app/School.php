<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\School
 *
 * @property string $id 代碼
 * @property string $name 學校名稱
 * @property string $address 學校地址
 * @property string $website 學校網址
 * @property string $phone 學校電話
 * @property string|null $student_email
 * @property int $type 公(0)/私(1)立
 * @property int $open 是否開放報名
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SchoolDept[] $depts
 * @method static \Illuminate\Database\Eloquent\Builder|\App\School whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\School whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\School whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\School whereOpen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\School wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\School whereStudentEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\School whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\School whereWebsite($value)
 * @mixin \Eloquent
 */
class School extends Model
{
    protected $table = 'school';
    public $incrementing = false;
    protected $keyType = 'string';

    public function depts()
    {
        return $this->hasMany('App\SchoolDept', 'school_id', 'id');
    }
}
