<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Role;

class AsiaCodeSetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asiacode:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Asia Code System Setup';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Start DB Migrate...');
        $this->call('migrate');
        $this->line('End DB Migrate');



        $student = new Role();
        $student->name         = 'student';
        $student->display_name = '學生';
        $student->description  = '一般學生';
        if ($student->save()) {
            $this->info('Role: student created!');
        } else {
            $this->error('Fail create Role: student');
        }

        $student_ban = new Role();
        $student_ban->name         = 'student_ban';
        $student_ban->display_name = '學生(封鎖)';
        $student_ban->description  = '(封鎖)學生';
        if ($student_ban->save()) {
            $this->info('Role: student_ban created!');
        } else {
            $this->error('Fail create Role: student_ban');
        }

        $teacher = new Role();
        $teacher->name         = 'teacher';
        $teacher->display_name = '教師';
        $teacher->description  = '教師';
        if ($teacher->save()) {
            $this->info('Role: teacher created!');
        } else {
            $this->error('Fail create Role: teacher');
        }

        $dept = new Role();
        $dept->name         = 'dept';
        $dept->display_name = '學系';
        $dept->description  = '學系';
        if ($dept->save()) {
            $this->info('Role: dept created!');
        } else {
            $this->error('Fail create Role: dept');
        }

        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = '管理者';
        $admin->description  = '管理者';
        if ($admin->save()) {
            $this->info('Role: admin created!');
        } else {
            $this->error('Fail create Role: admin');
        }

    }
}
