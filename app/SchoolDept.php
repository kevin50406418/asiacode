<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\SchoolDept
 *
 * @property string $id 科系代碼
 * @property string $name 科系名稱
 * @property string $school_id 學校代碼
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SchoolDept whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SchoolDept whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SchoolDept whereSchoolId($value)
 * @mixin \Eloquent
 */
class SchoolDept extends Model
{
    protected $table = 'school_dept';
    public $incrementing = false;
    protected $keyType = 'string';
}
