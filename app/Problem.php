<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Problem
 *
 * @property int $problem_id
 * @property string $title
 * @property string|null $description
 * @property string|null $input
 * @property string|null $output
 * @property string|null $hint
 * @property string|null $source
 * @property int $time_limit
 * @property int $memory_limit
 * @property string $defunct
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereDefunct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereHint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereInput($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereMemoryLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereOutput($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereProblemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereTimeLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Problem whereTitle($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ProblemSample[] $samples
 */
class Problem extends Model
{
    protected $table = 'problem';

    public function samples()
    {
        return $this->hasMany('App\ProblemSample', 'problem_id', 'problem_id');
    }
}
