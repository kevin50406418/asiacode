<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ProblemSample
 *
 * @property int $sample_id
 * @property int $problem_id
 * @property string $sample_input
 * @property string $sample_output
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProblemSample whereProblemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProblemSample whereSampleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProblemSample whereSampleInput($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProblemSample whereSampleOutput($value)
 * @mixin \Eloquent
 */
class ProblemSample extends Model
{
    protected $table = 'problem_sample';
}
