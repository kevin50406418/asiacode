<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\ExamSignup
 *
 * @property int $id
 * @property int $exam_id 考試編號
 * @property int $user_id 使用者編號
 * @property int $lang_id 程式語言編號
 * @property string $name_zh_tw 中文姓名
 * @property string $last_name_en 英文姓
 * @property string $first_name_en 英文名
 * @property string $school_id 學校代碼
 * @property string $dept_id 學系代碼
 * @property string $student_id 學號
 * @property string $edu_level 學制
 * @property string|null $pc2_account PC 2 帳號
 * @property string|null $pc2_password PC 2 密碼
 * @property float|null $solve_problem 解題數, -1 為沒來考試
 * @property string|null $cert_id 證書字號
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property int $sex 性別
 * @property string $id_number 身分證
 * @property string|null $cert_key_1 證書金鑰
 * @property string|null $cert_key_2 證書金鑰2
 * @property-read \App\CodingLang $code
 * @property-read \App\SchoolDept $dept
 * @property-read \App\School $school
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\ExamSignup onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereCertId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereCertKey1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereCertKey2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereDeptId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereEduLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereExamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereFirstNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereIdNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereLangId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereLastNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereNameZhTw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup wherePc2Account($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup wherePc2Password($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereSchoolId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereSolveProblem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamSignup whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ExamSignup withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\ExamSignup withoutTrashed()
 * @mixin \Eloquent
 */
class ExamSignup extends Model
{
    use SoftDeletes;

    protected $table = 'exam_signup';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'exam_id',
        'user_id',
        'name_zh_tw',
        'last_name_en',
        'first_name_en',
        'school_id',
        'dept_id',
        'edu_level',
        'pc2_account',
        'pc2_password',
        'lang_id',
        'solve_problem',
        'cert_id',
    ];

    protected $hidden = [
        'pc2_account', 'pc2_password'
    ];

    public function school()
    {
        return $this->hasOne('App\School', 'id', 'school_id');
    }

    public function dept()
    {
        return $this->hasOne('App\SchoolDept', 'id', 'dept_id');
    }

    public function code()
    {
        return $this->hasOne('App\CodingLang', 'id', 'lang_id');
    }
}
