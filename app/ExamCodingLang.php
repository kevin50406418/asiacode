<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ExamCodingLang
 *
 * @property int $exam_id 考試編號
 * @property int $lang_id 程式語言編號
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\CodingLang $lang
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamCodingLang whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamCodingLang whereExamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamCodingLang whereLangId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamCodingLang whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ExamCodingLang extends Model
{
    protected $table = 'exam_coding_lang';
    protected $primaryKey = ['exam_id','lang_id'];
    public $incrementing = false;

    protected $fillable = [
        'exam_id',
        'lang_id',
    ];

    public function lang()
    {
        return $this->hasOne('App\CodingLang', 'id', 'lang_id');
    }
}
