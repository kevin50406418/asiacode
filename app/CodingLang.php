<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CodingLang
 *
 * @property int $id
 * @property string $lang 程式語言名稱
 * @property string $slug 程式語言代碼
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CodingLang whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CodingLang whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CodingLang whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CodingLang whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CodingLang whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CodingLang extends Model
{
    protected $table = 'coding_lang';

    protected $fillable = [
        'lang',
        'slug',
    ];
}
