<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\GradeAbility
 *
 * @property int $id
 * @property int $exam_id 考試編號
 * @property string $slug 階級代碼
 * @property string $cert_title_en
 * @property string $cert_title_zh_tw
 * @property string $desc 能力指標說明
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GradeAbility whereCertTitleEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GradeAbility whereCertTitleZhTw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GradeAbility whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GradeAbility whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GradeAbility whereExamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GradeAbility whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GradeAbility whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GradeAbility whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class GradeAbility extends Model
{
    protected $table = 'grade_ability';
    protected $fillable = [
        'exam_id',
        'slug',
        'cert_title_en',
        'cert_title_zh_tw',
        'desc',
    ];
}
