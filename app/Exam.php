<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Exam
 *
 * @property int $id
 * @property string $name 名稱
 * @property string $place 考試地點
 * @property string $desc 考試說明
 * @property \Carbon\Carbon $time_start 考試時間：開始時間
 * @property \Carbon\Carbon $time_end 考試時間：結束時間
 * @property \Carbon\Carbon $signup_start 報名時間：開始時間
 * @property \Carbon\Carbon $signup_end 報名時間：截止時間
 * @property float|null $pass_threshold 通過門檻
 * @property \Carbon\Carbon $publish_time 成績公布時間
 * @property string $problem_note 考題註記
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ExamCodingLang[] $code
 * @property-read mixed $can_signup
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam wherePassThreshold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam wherePlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereProblemNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam wherePublishTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereSignupEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereSignupStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereTimeEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereTimeStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Exam extends Model
{
    protected $table = 'exam';
    protected $appends = ['image', 'can_signup'];
    protected $fillable = [
        'name',
        'place',
        'desc',
        'time_start',
        'time_end',
        'signup_start',
        'signup_end',
        'pass_threshold',
        'publish_time',
        'problem_note',
    ];

    protected $dates = [
        'time_start',
        'time_end',
        'signup_start',
        'signup_end',
        'publish_time',
    ];

    public function code()
    {
        return $this->hasMany('App\ExamCodingLang', 'exam_id', 'id');
    }

    public function getCanSignupAttribute()
    {
        $signup_start = $this->attributes[ 'signup_start' ];
        $signup_end   = $this->attributes[ 'signup_end' ];

        $signup_start = \Carbon\Carbon::parse($signup_start);
        $signup_end   = \Carbon\Carbon::parse($signup_end);

        return \Carbon\Carbon::now()->between($signup_start, $signup_end);

    }
}
